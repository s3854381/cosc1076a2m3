#ifndef COSC1076_ASSIGN2_AZULSTDIO_H
#define COSC1076_ASSIGN2_AZULSTDIO_H

#include "AzulStdIn.h"
#include "AzulStdOut.h"

/**
 * Contains methods for writing to and reading from standard input and output,
 * as well as files.
 */
class AzulStdIO {
public:

    AzulStdIO();
    AzulStdIO(const AzulStdIO& other);
    AzulStdIO(AzulStdIO&& other);
    ~AzulStdIO();

    /**
     * Gets a string from standard input that has been previously stored from the
     * stringPrompt() method.
     */
    std::string getString();

    /**
     * Main menu prompt. Contains options for NEW_GAME, LOAD_GAME, CREDITS and QUIT.
     */
    PromptOption getMenuOption();

    /**
     * Author: Michael Asquith
     * Created: 2020-09-24
     * 
     * Prompts the player for their choice of what to do on their turn. Valid inputs
     * are "turn", "save", and the end of file character.
     * 
     * Returns the PromptOptions TURN, SAVE or QUIT respectively.
     */
    PromptOption getTurnOption();

    /**
     * Returns a pointer to a Turn from the information already in standard input.
     * 
     * The stored arguments were retrieved from standard input when the turn option
     * was last in standard input. If the arguments are not of the form <int> <char>
     * <int>, then a std::logic_error will be thrown.
     */
    std::shared_ptr<Turn> getTurn();

    /**
     * Loads a savedGame from a file. The filename is the next string in standard input.
     * 
     * Returns a SavedGame of the selected file. Unknown error handling.
     */
    std::shared_ptr<SavedGame> loadGame(std::string filename);

    /**
     * Saves savedGame to a file. The filename is the next string in standard input.
     * 
     * Returns true if the file was successfully saved, and false otherwise.
     */
    bool saveGame(const SavedGame& savedGame);

    /**
     * Prints a line to standard output. 
     */
    void println(std::string string);

    /**
     * Prints a blank line to standard output. 
     */
    void println();

    /**
     * Prompts the user for a string. Returns STRING if a string has been
     * entered, or QUIT if eof() is reached.
     */
    PromptOption stringPrompt();
  
    /**
     * Outputs the current state of the Tile bag.
     */
    void outputTileBag(TileBag& tileBag);

    /**
     * Outputs the current state of the Factory.
     */
    void outputFactories(std::vector<std::shared_ptr<Factory>>& factories);

    /**
     * Outputs the current state of a given players pattern line and mosaic.
     */
    void outputPlayerBoard(Player& player);
    
    /**
     * Outputs the players current points.
     */
    void outputPlayerScore(Player& player);

    /**
     * Outputs the end of game scores, and the winner.
     */    
    void outputEndGame(std::vector<std::shared_ptr<Player>>& players);
    
    /**
     * Prints the game's credits.
     */
    void printCredits();
    
private:
    std::unique_ptr<AzulStdIn> azulStdIn;
    std::unique_ptr<AzulStdOut> azulStdOut;

    InputMap const TURN_OPTIONS = {
        {"turn", TURN},
        {"save", SAVE},
        {"quit", QUIT}
    };

    InputMap const MENU_OPTIONS = {
        {"1", START_GAME},
        {"2", LOAD_GAME},
        {"3", CREDITS},
        {"4", QUIT}
    };

    /**
     * Author: Michael Asquith
     * Created: 2020-09-19
     * 
     * Prompts standard input for a string matching a key in inputMap. If a match
     * is found, it returns inputMap's value associated with that key. If no match
     * is found, it will prompt the user for input again.
     * 
     * If the end of file character is read, PromptOption.QUIT is returned.
     */
    PromptOption optionPrompt(const InputMap& inputMap);

    /**
     * Prints the output needed before the user is prompted.
     */
    void printPrompt();
};

#endif // COSC1076_ASSIGN2_AZULSTDIO_H