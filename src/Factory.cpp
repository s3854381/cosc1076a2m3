#include "Factory.h"
#include <iostream>
#include <algorithm>

Factory::Factory(){
}

Factory::Factory(const Factory& other){
    this->factory = other.factory;
}

Factory::Factory(Factory&& other){
    factory = other.factory;
}

Factory::~Factory(){
    clear();
}

unsigned int Factory::size(){
    return factory.size();
}

void Factory::addTile(std::shared_ptr<Tile> tile){
    factory.insert(tile);
}

void Factory::removeTileByColor(TileColor color){
    factory.removeAll(Tile(color));
}

std::vector<std::shared_ptr<Tile>> Factory::getColorGroup(TileColor color){
    return factory.removeAll(Tile(color));
}

void Factory::clear(){
    factory.clear();
}

bool Factory::containsFirstToken() {
    return (factory.count(Tile(FIRST_TOKEN)) != 0);
}

bool Factory::validColorGroup(TileColor color) {
    bool validGroup = factory.count(Tile(color)) != 0;

    if (!validGroup) {
        throw std::invalid_argument("Invalid arguement. Factory is empty or factory does not contain the specified tile.");
    }

    return validGroup;
}

unsigned int Factory::countTiles(TileColor color) {
    return factory.count(Tile(color));
}

const TilePtrVector Factory::getAll() const {
    return factory.getAll();
}