#include "AIStrategy.h"

unsigned int CountWantedVector::getMaxCount() {
    unsigned int max = 0;
    for (const unsigned int& count : lineCounts) {
        if (count > max) {
            max = count;
        }
    }
    return max;
}

unsigned int CountWantedVector::getBrokenFit(unsigned int count) {
    bool found = false;
    unsigned int bestIndex = 0;
    unsigned int fewestBroken = std::numeric_limits<unsigned int>::max();
    for (unsigned int i = 0; i < lineCounts.size(); ++i) {
        unsigned int currentLineCount = lineCounts.at(i);
        if (currentLineCount != 0) {
        unsigned int broken = count - currentLineCount;
            if (broken < fewestBroken) {
                fewestBroken = broken;
                bestIndex = i;
                found = true;
            }
        }
    }
    // If a line has been found, add one to account for the floor line at index 0
    if (found) {
        bestIndex += 1;
    }
    return bestIndex;
}


unsigned int CountWantedVector::getUnbrokenFit(unsigned int count) {

    unsigned int bestIndex = 0;
    unsigned int bestSpace = std::numeric_limits<unsigned int>::max();
    for (unsigned int i = 0; i < lineCounts.size(); ++i) {
        unsigned int currentLineCount = lineCounts.at(i);
        if (count <= currentLineCount) {
            unsigned int space = currentLineCount - count;
            if (space < bestSpace) {
                bestSpace = space;
                bestIndex = i;
            }
        }
    }
    // Add one to account for the floor line at index 0.
    // Because this method does not break non-first tiles, it will never return 0.
    return bestIndex + 1;
}

AIStrategy::AIStrategy(std::vector<std::shared_ptr<Factory>> factories) {
    this->factories = factories;
    this->player = player;

    // Should take colors from a Tile or maybe TileUtils or Types file.
    for (const TileColor& t : {RED, YELLOW, DARK_BLUE, LIGHT_BLUE, BLACK}) {
        allColors.push_back(t);
    }

    for (const TileColor& t : allColors) {
        std::vector<unsigned int> lineCounts = std::vector<unsigned int>(PATTERN_LINES_ROWS, 0);
        CountWantedVector cwv;
        cwv.lineCounts = lineCounts;
        countVectors.insert({t, cwv});
    }
}

AIStrategy::AIStrategy(const AIStrategy& other) {
    for(const std::shared_ptr<Factory>& factory : other.factories) {
        std::shared_ptr<Factory> newFactory;
        newFactory.reset(new Factory(*factory));
        factories.push_back(newFactory);
    }

    player.reset(new Player(*other.player));

    allColors = other.allColors;

    countVectors = std::unordered_map<TileColor, CountWantedVector>(other.countVectors);

}

AIStrategy::AIStrategy(AIStrategy&& other) {
    for(const std::shared_ptr<Factory>& factory : other.factories) {
        std::shared_ptr<Factory> newFactory;
        newFactory.reset(new Factory(*factory));
        factories.push_back(newFactory);
    }
    other.factories.clear();

    player = other.player;
    other.player = nullptr;

    allColors = other.allColors;

    countVectors = std::unordered_map<TileColor, CountWantedVector>(other.countVectors);
}

AIStrategy::~AIStrategy() {
    factories.clear();
    player = nullptr;
}

void AIStrategy::setPlayer(std::shared_ptr<Player> player) {
    this->player = player;
    updateCountVectors();
}


std::shared_ptr<Turn> AIStrategy::getTurn() {

    // Assume each tile color has counts wanted
    updateCountVectors();
    
    unsigned int returnFactoryIndex;
    TileColor returnTileColor;
    int returnLine;

    // Non-breaking tiles variables        
    unsigned int mostTiles = 0;
    unsigned int mostFactoryIndex = 0;
    TileColor mostColor = EMPTY;
    unsigned int mostLine = 0;

    // Breaking tiles variables
    unsigned int minTiles = std::numeric_limits<unsigned int>::max();
    unsigned int minFactoryIndex = 0;
    TileColor minColor = EMPTY;
    unsigned int minLine = 0;

    // Taking tiles
    unsigned int factoryIndex = 0;
    for (const std::shared_ptr<Factory>& factory : factories) {
        for (const TileColor& color : allColors) {
            unsigned int count = factory->countTiles(color);
            CountWantedVector countVector = countVectors.at(color);
            // Priority 1: Take as many tiles as possible less than some color's wanted
            if (count > mostTiles && count <= countVector.getMaxCount()) {
                mostTiles = count;
                mostFactoryIndex = factoryIndex;
                mostColor = color;

                // Put in unfinished line, or if none the best fit
                mostLine = countVector.getUnbrokenFit(count);
            }
            // Priority 2: Take as few tiles as possible
            if (count > 0 && count < minTiles) {
                minTiles = count;
                minFactoryIndex = factoryIndex;
                minColor = color;

                // Put in unfinished line, or if none break as few as possible
                minLine = countVector.getBrokenFit(count);
            }

        }
        ++factoryIndex;
    }
    
    // If there are tiles that can be taken without breaking any, mostTiles will be greater than 0
    if (mostTiles > 0) {
        returnFactoryIndex = mostFactoryIndex;
        returnTileColor = mostColor;
        returnLine = mostLine;
    } else {
        returnFactoryIndex = minFactoryIndex;
        returnTileColor = minColor;
        returnLine = minLine;
    }
    char returnColor = Tile(returnTileColor).toChar();

    std::shared_ptr<Turn> returnValue;
    returnValue.reset(new Turn(returnFactoryIndex, returnColor, returnLine));

    return returnValue;
}

std::string AIStrategy::getName() {
    return "Shallow Blue";
}

void AIStrategy::updateCountVectors() {

    for(const TileColor& color : allColors) {

        bool startedLine = false;
        std::vector<unsigned int>& lineCounts = countVectors.at(color).lineCounts;
        
        for (unsigned int i = 0; i < lineCounts.size() && !startedLine; ++i) {
            try {
                if (player->validPatternLine(i, color)) {
                    lineCounts.at(i) = player->freeSpaceInLine(i);

                    // If this line has already started, end the loop at set all others to zero.
                    if (player->patternLineStarted(i)) {
                        startedLine = true;
                        for (unsigned int j = 0; j < lineCounts.size(); ++j) {
                            if (j != i) {
                                lineCounts.at(j) = 0;
                            }
                        }
                    }
                }
                
            } catch (std::invalid_argument& e) {
                // freeSpaceInLine throws std::invalid_argument if tile is not valid for that line
                lineCounts.at(i) = 0;
            }
        }
    }
}