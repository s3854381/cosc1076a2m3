#include "AzulStdIn.h"

AzulStdIn::AzulStdIn() {
    storedArgs = std::queue<std::string>();
}

AzulStdIn::AzulStdIn(const AzulStdIn& other) {
    storedArgs = std::queue<std::string>(other.storedArgs);
}

AzulStdIn::AzulStdIn(AzulStdIn&& other) {
    storedArgs = std::move(other.storedArgs);
}

AzulStdIn::~AzulStdIn() {}

PromptOption AzulStdIn::promptUser(const InputMap& inputMap) {
    PromptOption returnValue = QUIT;

    storeLineAsArgs(std::cin);

    // Calling End of File at any time should quit the game
    if (std::cin.eof()) {
        returnValue = QUIT;
    } else {
        if (storedArgs.size() == 0) {
            throw std::invalid_argument("No user option found");
        }
        returnValue = inputMap.at(popStoredArg());
    }

    return returnValue;
}

std::string AzulStdIn::getString() {
    std::string returnValue = "";
    if (storedArgs.size() > 0) {
        returnValue = popStoredArg();
    }
    return returnValue;
}

std::shared_ptr<Turn> AzulStdIn::getTurn() {
    std::shared_ptr<Turn> returnValue = nullptr;
    
    try {
        if (storedArgs.size() != 3) {
            throw std::invalid_argument("Incorrect number of arguments.");
        } else {
            std::string buffer = popStoredArg();
            unsigned int factoryIndex = std::stoi(buffer);

            buffer = popStoredArg();
            if (buffer.size() != 1) {
                throw std::invalid_argument("Tile must be a single character.");
            }
            char tileChar = buffer[0];

            buffer = popStoredArg();
            unsigned int playerLine = std::stoi(buffer);

            returnValue = std::make_shared<Turn>(factoryIndex, tileChar, playerLine);
        }
    } catch (std::logic_error& e) {
        throw std::logic_error("Invalid turn arguments.");
    }
    return returnValue;
}

std::shared_ptr<SavedGame> AzulStdIn::loadGame(std::string filename) {

    std::ifstream inFile(filename);

    std::shared_ptr<SavedGame> returnValue =
            std::make_shared<SavedGame>(readTileBag(inFile));
        
    std::shared_ptr<StringVector> playerNames = readPlayerNames(inFile);
    for (const std::string& player : *playerNames) {
        returnValue->addPlayer(player);
    }

    std::shared_ptr<TurnVector> turns = readTurns(inFile);
    for (const Turn& turn : *turns)
    {
        returnValue->addTurn(turn);
    }
    inFile.close();

    return returnValue;
}

void AzulStdIn::storeLineAsArgs(std::istream& istream) {
    std::string buffer;
    getLine(istream, buffer);
    clearStoredArgs();

    int argStart;
    bool wordStarted = false;
    for(unsigned int i = 0; i != buffer.size(); ++i) {
        if(buffer[i] != ' ') {
            if(!wordStarted) {
                wordStarted = true;
                argStart = i;
            }
        } 
        else { // buffer[i] == ' '
            if (wordStarted) {
                storedArgs.push(buffer.substr(argStart, i - argStart));
            }
            wordStarted = false;
        }
    }
    // If the line does not end in a space, the last string will not yet be added.
    if(wordStarted) {
        storedArgs.push(buffer.substr(argStart, buffer.size() - argStart));
    }
}

std::string AzulStdIn::popStoredArg() {
    std::string returnValue = storedArgs.front();
    storedArgs.pop();
    return returnValue;
}

std::shared_ptr<TileVector> AzulStdIn::readTileBag(std::ifstream& ifstream) {
    std::string buffer;
    std::shared_ptr<TileVector> returnValue = std::make_shared<TileVector>();
    getLine(ifstream, buffer);
    for (const char& c : buffer)
    {
        returnValue->push_back(Tile(c));
    }
    return returnValue;
}

std::shared_ptr<StringVector> AzulStdIn::readPlayerNames(std::ifstream& ifstream) {
    std::shared_ptr<StringVector> returnValue = std::make_shared<StringVector>();

    for(int i = 0; i < NUM_PLAYERS; ++i) {
        std::string buffer;
        getLine(ifstream, buffer);
        if (buffer.back() == '\r') {
            buffer.pop_back();
        }
        returnValue->push_back(buffer);
    }

    return returnValue;
}

std::shared_ptr<TurnVector> AzulStdIn::readTurns(std::ifstream& ifstream) {
    std::shared_ptr<TurnVector> returnValue = std::make_shared<TurnVector>();

    while(!ifstream.eof()) {
        storeLineAsArgs(ifstream);

        if (!ifstream.eof()) {
            if (storedArgs.front() != "turn") {
                throw std::runtime_error("Can only read turn actions from file.");
            }
            storedArgs.pop();
            returnValue->push_back(*getTurn());
        }
    }
    return returnValue;
}

PromptOption AzulStdIn::stringPrompt() {
    PromptOption returnValue = QUIT;
    std::string buffer;
    getLine(std::cin, buffer);
    clearStoredArgs();

    if (std::cin.eof()) {
        returnValue = QUIT;
    } else if (buffer.size() > 0) {
        returnValue = STRING;
        storedArgs.push(buffer);
    } else {
        throw std::invalid_argument("No string found.");
    }

    return returnValue;
}

void AzulStdIn::getLine(std::istream& istream, std::string& string) {
    std::getline(istream, string);
    if (string.back() == '\r') {
        string.pop_back();
    }
}

void AzulStdIn::clearStoredArgs() {
    while(storedArgs.size() > 0)
    storedArgs.pop();
}