#ifndef COSC1076_ASSIGN2_AZULSTDOUT_H
#define COSC1076_ASSIGN2_AZULSTDOUT_H

#include <iostream>
#include <fstream>

#include "Factory.h"
#include "Player.h"
#include "SavedGame.h"

/**
 * Azul output methods using std::cout. Also includes file writing methods.
 */
class AzulStdOut {
public:
    AzulStdOut();
    AzulStdOut(const AzulStdOut& other);
    AzulStdOut(AzulStdOut&& other);
    ~AzulStdOut();

    /**
     * Saves savedGame to a file. The filename is the next string in standard input.
     * 
     * Returns true if the file was successfully saved, and false otherwise.
     */
    bool saveGame(const SavedGame& savedGame, std::string filename);

    /**
     * Prints a line to standard output. 
     */
    void println(std::string string) { std::cout << string << std::endl; }
    void println() { std::cout << std::endl; }

    /*
     * Prints the beginning of the line for a user prompt.
     */
    void printPrompt() { std::cout << "> "; }
  
    /**
     * Outputs the current state of the Tile bag. Used for testing purposes.
     */
    void outputTileBag(TileBag& tileBag);

    /**
     * Outputs the current state of the Factory.
     */
    void outputFactories(std::vector<std::shared_ptr<Factory>>& factories);

    /**
     * Outputs the current state of a given players pattern line and mosaic.
     */
    void outputPlayerBoard(Player& player);
    
    /**
     * Outputs the players current points.
     */
    void outputPlayerScore(Player& player);

    /**
     * Outputs the end of game scores, and the winner.
     */    
    void outputEndGame(std::vector<std::shared_ptr<Player>>& players);

    /**
     * Prints the game's credits.
     */
    void printCredits();

    /**
     * Prints the game's menu options.
     */
    void printMenu();

private:

    /**
     * Writes one line to ofstream, with each tile in tileBag represented by
     * one character.
     */
    void writeTileBag(std::ofstream& ofstream, const TileVector& tileBag);

    /**
     * Writes each player name in playerNames on its own line in ofstream, in
     * order.
     */
    void writePlayerNames(std::ofstream& ofstream, const StringVector& playerNames);

    /**
     * Writes each turn in turns to ofstream, one line each. Uses the format:
     *      turn i c j
     * where i is the factory number, c is the tile character, and j is the
     * player line number.
     */
    void writeTurns(std::ofstream& ofstream, const TurnVector& turns);
};

#endif // COSC1076_ASSIGN2_AZULSTDOUT_H