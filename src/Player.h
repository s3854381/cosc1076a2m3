#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <array>
#include <memory>
#include <exception>
#include "PlayerBoard.h"

/**
 * The Player class stores the information about a player in the game. 
 */
class Player {
public:
    Player(std::string name);
    Player(std::string name, char templateType);
    Player(const Player& other);
    Player(Player&& other);
    ~Player();

    /**
     * Returns the name of the player.
     */
    std::string getName();
    /**
     * Returns the amount of points the player has.
     */
    int getPoints();
    /**
     * Adds the amount of points specified to the total amount of points the player has.
     */
    void addPoints(int points);
    /**
     * Returns the playerMosaic.
     */
    std::array<std::array<std::shared_ptr<Tile>, MOSAIC_COLUMNS>, MOSAIC_ROWS> getPlayerMosaic();
    /**
     * Returns the mosaicTemplate.
     */
    std::array<std::array<std::shared_ptr<Tile>, MOSAIC_COLUMNS>, MOSAIC_ROWS> getMosaicTemplate();
    /**
     * Returns the current state of the floorLine.
     */
    std::vector<std::shared_ptr<Tile>> getFloorLine();
    /**
     * Adds the specified number and colour of tiles to the playerLine in the playerBoard.
     */
    void patternLineAdd(unsigned int row, std::vector<std::shared_ptr<Tile>>& colourGroup);
    /**
     * Returns the current state of the pattern lines of the player's board.
     */
    std::array<std::vector<std::shared_ptr<Tile>>, PATTERN_LINES_ROWS> getPatternLines();
    /**
     * Checks to see if a player has the first tile in their floor line.
     */
    bool checkIfFirst();
    /**
     * Adds the first token to the floor line in the board.
     */
    void addFirstToken(std::vector<std::shared_ptr<Tile>>& colourGroup);
    /**
     * Adds the specified amount of the tile to the floor line.
     */
    void floorLineAdd(std::vector<std::shared_ptr<Tile>>& colourGroup);
    /**
     * Performs end of round functions such as calling the player board end of round method as well as changing the points based on scoring.
     */
    void endOfRound(TileBag& tileBag);
    /**
     * Sets first token to false
     */
    void resetFirstToken();
    /**
     * Returns true if the pattern line at "row" is not full.
     * Returns false if the pattern at "row" is full
     * Throws invalid arguement if false.
     */
    bool validPatternLine(unsigned int row, TileColor colour);
    /**
     * Returns the number of free spaces in the pattern line specified by row.
     */
    unsigned int freeSpaceInLine(unsigned int row);
    /**
     * Returns true if the pattern line given by row has at least one tile in it.
     */
    bool patternLineStarted(unsigned int row);


private:
    std::string name;
    int points;
    std::unique_ptr<PlayerBoard> board;
    bool firstToken;
};

#endif //PLAYER_H