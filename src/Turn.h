#ifndef COSC1076_ASSIGN2_TURN_H
#define COSC1076_ASSIGN2_TURN_H

/**
 * Storage class that records the factory number, tile type and
 * player line for an Azul move. 
 */
class Turn {
public:
    Turn(unsigned int factory, char tile, unsigned int playerline);
    Turn(const Turn& other);
    Turn(Turn&& other);
    ~Turn();

    /**
     * Returns the factory choice as an integer.
     */
    unsigned int getFactory() const;
    /**
     * Returns a character of the Tile choice.
     */
    char getTileChar() const;
    /**
     * Returns an unsigned integer of the Pattern Line choice.
     */
    unsigned int getPlayerLine() const;

private:
    unsigned int factory;
    char tileChar;
    unsigned int playerLine;
};

#endif // COSC1076_ASSIGN2_TURN_H