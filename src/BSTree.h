#ifndef COSC1076_ASSIGN2_BSTREE_H
#define COSC1076_ASSIGN2_BSTREE_H

#include <exception>
#include <iostream>
#include <memory>
#include <vector>

#include "Tile.h"

class BSNode;
typedef std::shared_ptr<BSNode>     BSNodePtr;
typedef std::shared_ptr<Tile>       TilePtr;
typedef std::vector<TilePtr>        TilePtrVector;

class BSNode {
public:
    BSNode(TilePtr data);
    BSNode(const BSNode& other);
    BSNode(BSNode&& other);
    ~BSNode();

    TilePtr data;
    BSNodePtr left = nullptr;
    BSNodePtr right = nullptr;

    std::string toString();
};

class BSTree {
public:
    BSTree();
    BSTree(const BSTree& other);
    BSTree(BSTree&& other);
    ~BSTree();

    BSTree& operator=(const BSTree& other);

    /**
     * Returns the total number of elements in the tree, by summing the counts of the nodes.
     */
    unsigned int size() const;

    /**
     * Returns the number of items identical to what is pointed to by reference, that are
     * stored in the tree.
     */
    unsigned int count(const Tile& data) const;

    /**
     * Inserts a Tile into the tree.
     */
    void insert(TilePtr dataPtr);

    /**
     * Returns and removes a single Tile from the tree, matching the given data.
     * If no matching Tile is found, returns a nullptr.
     */
    TilePtr extract(const Tile& data);

    /**
     * Removes all items in the BSTree and returns a vector of pointers to those Tiles.
     */
    TilePtrVector removeAll();

    /**
     * Removes all items identical to dataPtr and returns a vector of pointers to those Tiles.
     */
    TilePtrVector removeAll(const Tile& data);

    /**
     * Gets an immutable vector of points to all items in the BSTree.
     */
    const TilePtrVector getAll() const;

    /**
     * Deletes all items in the tree.
     */
    void clear();

    /**
     * Prints all items in the BSTree. Used for debugging.
     */
    void print();

    /**
     * Returns true if this is a valid BSTree i.e.:
     *    - all left children are less than their parent, or null
     *    - all right children are greater than or equal to their parent, or null
     */
    bool validate();


private:
    BSNodePtr root;
    unsigned int length;

    unsigned int count(const Tile& data, BSNodePtr node) const;
    BSNodePtr insert(TilePtr dataPtr, BSNodePtr node);
    TilePtr extract(const Tile& data, BSNodePtr& node);
    TilePtr extractNode(BSNodePtr& node);
    bool validate(BSNodePtr node, TilePtr min, TilePtr max);
    const TilePtrVector getAll(BSNodePtr node) const;

    void print(BSNodePtr node, std::string chain);
};

#endif //COSC1076_ASSIGN2_BSTREE_H