#ifndef LINKED_LIST_TEMPLATE_H
#define LINKED_LIST_TEMPLATE_H

#include <memory>
#include <exception>
#include <vector>

template<typename T>
class Node {
public:
   Node(std::shared_ptr<T> data, std::shared_ptr<Node<T>> next);

   std::shared_ptr<T> data;
   std::shared_ptr<Node<T>> next;
};

/**
 * Templated implementation of a single-linked list
 * 
 * Needs to be thoroughly tested
 * 
 * Stores a pointer to the list tail, and stores the size of the list.
 * Because of the tail pointer, addBack is O(1), however removeBack is
 * still O(n)
 * 
 * 
 * Author: Michael Asquith
 * Date: 2020-09-09
 */
template<typename T>
class LinkedList {
public:
   LinkedList();
   LinkedList(const LinkedList& other);
   LinkedList(LinkedList&& other);
   ~LinkedList();
   
   /**
    * Return the current size of the Linked List as an unsigned int.
    */
   unsigned int size() const;
   /**
    * Get the value at the given index.
    */
   std::shared_ptr<T> get(const unsigned int index) const;
   /**
    * Add the value to the back of the Linked List.
    */
   void addBack(std::shared_ptr<T> data);
   /**
    * Add the value to the front of the Linked List.
    */
   void addFront(std::shared_ptr<T> data);
   /**
    * Remove the value at the back of the Linked List.
    */
   void removeBack();
   /**
    * Remove the value at the front of the Linked List.
    */
   void removeFront();
   /**
    * Removes all values from the Linked List.
    */
   void clear();
   /**
    * Returns a pointer to an ordered vector of deep copies of all items in the Linked List. 
    */
   std::shared_ptr<std::vector<T>> getVector();

private:

   std::shared_ptr<Node<T>> head;
   std::shared_ptr<Node<T>> tail;
   unsigned int length;
};

// Class Definitions

template<typename T>
Node<T>::Node(std::shared_ptr<T> data, std::shared_ptr<Node<T>> next) :
   data(data),
   next(next)
{}

template<typename T>
LinkedList<T>::LinkedList() 
{
   head = nullptr;
   tail = nullptr;
   length = 0;
}

template<typename T>
LinkedList<T>::LinkedList(const LinkedList& other)
{
   head = nullptr;
   tail = nullptr;
   length = 0;
   std::shared_ptr<Node<T>> current = other.head;
   while (current != nullptr)
   {
      addBack(current->data);
      current = current->next;
   }
}

template<typename T>
LinkedList<T>::LinkedList(LinkedList&& other)
{
   head = other.head;
   tail = other.tail;
   length = other.length;
   other.head = nullptr;
   other.tail = nullptr;
   other.length = 0;
}

template<typename T>
LinkedList<T>::~LinkedList() {
   clear();
}

template<typename T>
unsigned int LinkedList<T>::size() const {
   return length;
}

template<typename T>
std::shared_ptr<T> LinkedList<T>::get(const unsigned int index) const {
   std::shared_ptr<T> returnValue;
   if (index < size()) {
      std::shared_ptr<Node<T>> current = head;
      for(unsigned int i = 0; i != index; ++i) {
         current = current->next;
      }
      returnValue = current->data;
   }
   else {
      throw std::out_of_range("Linked List get - index out of range.");
   }
   return returnValue;
}

template<typename T>
void LinkedList<T>::addFront(std::shared_ptr<T> data) {
   head = std::make_shared<Node<T>>(data, head);
   if (size() == 0) {
      tail = head;
   }
   ++length;
}

template<typename T>
void LinkedList<T>::addBack(std::shared_ptr<T> data) {
   if (size() == 0) {
      head = std::make_shared<Node<T>>(data, nullptr);
      tail = head;
   }
   else {
      tail->next = std::make_shared<Node<T>>(data, nullptr);
      tail = tail->next;
   }
   ++length;
}

template<typename T>
void LinkedList<T>::removeBack() {
   //need a pointer to the current node and another to the previous node.
   std::shared_ptr<Node<T>> current = head;
   // Length 0
   if (current != nullptr) {
      // Length 1
      if (current->next == nullptr) {
         removeFront();
      }
      // Length 2+
      else {
         while (current->next->next != nullptr)
         {
            current = current->next;
         }
         current->next = nullptr;
         tail = current;
         --length;
      }
   }
}

template<typename T>
void LinkedList<T>::removeFront() {
   if (size() != 0) {
      head = head->next;

      --length;
      if (size() == 0) {
         tail = nullptr;
      }
   }
}

template<typename T>
void LinkedList<T>::clear() {
   head = nullptr;
   tail = nullptr;
   length = 0;
}

template<typename T>
std::shared_ptr<std::vector<T>> LinkedList<T>::getVector() {
   std::shared_ptr<std::vector<T>> vector = std::make_shared<std::vector<T>>();
   // Reserve space in vector for all Linked List items
   vector->reserve(length);

   std::shared_ptr<Node<T>> current = head;
   while (current != nullptr) {
      vector->push_back( T( *(current->data) ) );
      current = current->next;
   }

   return vector;
}

#endif // LINKED_LIST_TEMPLATE_H
