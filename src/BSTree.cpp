#include "BSTree.h"

BSNode::BSNode(TilePtr data) :
    data(data),
    left(nullptr),
    right(nullptr)
{}

BSNode::BSNode(const BSNode& other) {
    
    if (other.data != nullptr) {
        data.reset(new Tile(*other.data));
    } else {
        data = nullptr;
    }
    if (other.left != nullptr) {
        left.reset(new BSNode(*other.left));
    } else {
        left = nullptr;
    }
    if (other.right != nullptr) {
        right.reset(new BSNode(*other.right));
    } else {
        right = nullptr;
    }
}

BSNode::BSNode(BSNode&& other) {

    data = other.data;
    other.data = nullptr;

    if (other.left != nullptr) {
        left = other.left;
        other.left = nullptr;
    } else {
        left = nullptr;
    }
    if (other.right != nullptr) {
        right = other.right;
        other.right = nullptr;
    } else {
        right = nullptr;
    }
}

BSNode::~BSNode() {
    data = nullptr;
    left = nullptr;
    right = nullptr;
}

BSTree::BSTree() {
    root = nullptr;
    length = 0;
}

BSTree::BSTree(const BSTree& other) {
    root.reset(new BSNode(*other.root));
    length = other.length;
}

BSTree::BSTree(BSTree&& other) {
    root = other.root;
    other.root = nullptr;
    length = other.length;
    other.length = 0;
}

BSTree::~BSTree() {
    clear();
}

BSTree& BSTree::operator=(const BSTree& other) {
    if (other.root != nullptr) {
        root.reset(new BSNode(*other.root));
    } else {
        root = nullptr;
    }
    length = other.length;
    return *this;
}

unsigned int BSTree::size() const {
    return length;
}

unsigned int BSTree::count(const Tile& data) const {
    unsigned int returnValue = 0;
    if (root != nullptr) {
        returnValue = count(data, root);
    }
    return returnValue;
}

unsigned int BSTree::count(const Tile& data, BSNodePtr node) const {
    unsigned int returnValue = 0;
    
    if (node != nullptr) {
        Tile nodeData = *node->data;

        if (data < nodeData) {
            returnValue += count(data, node->left);

        } else {
            returnValue += count(data, node->right);
            if (data == nodeData) {
                ++returnValue;
            }
        }
    }
    return returnValue;
}

void BSTree::insert(TilePtr dataPtr) {
    root = insert(dataPtr, root);
}

BSNodePtr BSTree::insert(TilePtr dataPtr, BSNodePtr node) {

    if (node == nullptr) {
        node.reset(new BSNode(dataPtr));
        ++length;
    } else {
        Tile data = *dataPtr;
        Tile nodeData = *node->data;
        if (data < nodeData) {
            node->left = insert(dataPtr, node->left);
            
        } else {
            node->right = insert(dataPtr, node->right);
        }
    }
    return node;
}

TilePtrVector BSTree::removeAll() {
    TilePtrVector returnValue;
    while (root != nullptr) {
        returnValue.push_back(extractNode(root));
    }
    return returnValue;
}

TilePtrVector BSTree::removeAll(const Tile& data) {
    TilePtrVector returnValue;
    int iterations = count(data);
    for (int i = 0; i != iterations; ++i) {
        returnValue.push_back(extract(data));
    }
    return returnValue;
}

TilePtr BSTree::extract(const Tile& data) {

    TilePtr returnValue = nullptr;
    if (root != nullptr) {
        Tile rootData = *root->data;
        if (data < rootData) {
            returnValue = extract(data, root->left);
        } else if (data > rootData) {
            returnValue = extract(data, root->right);
        } else {
            // extract the root
            returnValue = extractNode(root);
        }
    }
    return returnValue;
}

TilePtr BSTree::extract(const Tile& data, BSNodePtr& node) {
    TilePtr returnValue = nullptr;

    if (node != nullptr) {
        Tile nodeData = *node->data;
        if (data < nodeData) {
            returnValue = extract(data, node->left);
        } else if (data > nodeData) {
            returnValue = extract(data, node->right);
        } else {
            returnValue = extractNode(node);
        }
    }

    return returnValue;
}

TilePtr BSTree::extractNode(BSNodePtr& node) {
    BSNodePtr nodeL = node->left;
    BSNodePtr nodeR = node->right;
    TilePtr returnValue = node->data;

    if (nodeR == nullptr) {
        node = nodeL;
        --length;
    } else {

        if (node->right->left == nullptr) {
            nodeR->left = nodeL;
            node = nodeR;
            --length;
        } else {

            BSNodePtr current = node->right;
            while (current->left->left != nullptr) {
                current = current->left;
            }
            BSNodePtr target = current->left;
            
            current->left = target->right;
            node.reset(new BSNode(extractNode(target)));
            node->right = nodeR;
            node->left = nodeL;
        }
    }
    return returnValue;
}

const TilePtrVector BSTree::getAll() const {
    return getAll(root);
}

const TilePtrVector BSTree::getAll(BSNodePtr node) const {
    TilePtrVector returnValue;

    if (node != nullptr) {
        TilePtrVector leftTree = getAll(node->left);
        TilePtrVector rightTree = getAll(node->right);
        returnValue.insert(returnValue.begin(), leftTree.begin(), leftTree.end());
        returnValue.push_back(node->data);
        returnValue.insert(returnValue.end(), rightTree.begin(), rightTree.end());
    }

    return returnValue;
}

void BSTree::clear() {
    root = nullptr;
    length = 0;
}

void BSTree::print() {
    print(root, "");
}

void BSTree::print(BSNodePtr node, std::string chain) {
    
    if (node == nullptr) {
        std::cout << chain << "nullptr" << std::endl;
    } else {
        chain += node->data->toChar();
        
        print(node->left, chain + " L> ");
        std::cout << chain << std::endl;
        print(node->right, chain + " R> ");
    }
}

bool BSTree::validate() {
    return validate(root, nullptr, nullptr);
}

bool BSTree::validate(BSNodePtr node, TilePtr min, TilePtr max) {
    bool returnValue = true;

    if (node == nullptr) {
        return true;
    } else {
        TilePtr nodeData = node->data;
        returnValue =
            (min == nullptr || *nodeData >= *min) &&
            (max == nullptr || *nodeData < *max) &&
            validate(node->left, min, nodeData) &&
            validate(node->right, nodeData, max);
    }
    return returnValue;
}