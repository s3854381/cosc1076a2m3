#include "AzulStdIO.h"

/*
*   Test and Review of LinkedList Implementation
*   Possible Test Cases:
*   1: Test Add front.
    2: Test Add back.
    3: Test Add front.
    4: Test get front.
    5: Test remove front
    6: Test remove back
    7: Test get back.
    8: Test copyconstructor (deep or shallow)
    3: Test clear() on empty linkedlist
    4: Test clear() on linkedlist.
*
*
*/

void startTest();
void endTest();
void println(std::string s);

int main(void) {

    AzulStdIO azulStdIO;

    //construct a linkedlist.
    //Test Case 1:
    startTest();
    {
        PromptOption p = azulStdIO.getTurnOption();
        if (p == TURN) {
            Turn turn = *azulStdIO.getTurn();
            println("TURN");
            std::cout << "Factory index: " << turn.getFactory() << std::endl;
            std::cout << "Tile char: " << turn.getTileChar() << std::endl;
            std::cout << "Player line: " << turn.getPlayerLine() << std::endl;
        }
    }
    endTest();

    startTest();
    {
        PromptOption p = azulStdIO.getTurnOption();
        if (p == SAVE)
        {
            println("SAVE");
        }
    }
    endTest();

    startTest();
    {
        std::shared_ptr<TileBag> t = std::make_shared<TileBag>();
        t->addBack(std::make_shared<Tile>(DARK_BLUE));
        t->addBack(std::make_shared<Tile>(LIGHT_BLUE));
        t->addBack(std::make_shared<Tile>(BLACK));
        t->addBack(std::make_shared<Tile>(RED));

        std::shared_ptr<SavedGame> s = std::make_shared<SavedGame>(*t);
        s->addPlayer("Alice");
        s->addPlayer("Bob");
        s->addTurn(2, 'B', 3);
        s->addTurn(3, 'L', 3);
        s->addTurn(2, 'Y', 2);
        azulStdIO.saveGame(*s);
    }
    endTest();

    startTest();
    {
        std::shared_ptr<SavedGame> s = azulStdIO.loadGame("Tests/stdioTestSave.save");
        for(const Tile& t : s->getInitialTileBag()) {
            std::cout << t.toChar();
        }
        std::cout << std::endl;
        for(const std::string& p : s->getPlayerNames()) {
            std::cout << p << std::endl;
        }
        for(const Turn& t : s->getTurns()) {
            std::cout << "turn " << t.getFactory() << " " << t.getTileChar() << " " << t.getPlayerLine() << std::endl;
        }
    }
    endTest();

    startTest();
    {
        StringVector names = StringVector();
        for(int i = 0; i < 3; ++i) {
            if (azulStdIO.stringPrompt() == STRING) {
                names.push_back(azulStdIO.getString());
            }
        }
        
        for(const std::string& s : names) {
            std::cout << s << std::endl;
        }
    }
    endTest();

    startTest();
    {
        PromptOption p = azulStdIO.getTurnOption();
        if (p == QUIT)
        {
            azulStdIO.println("QUIT");
        }
    }
    endTest();
    
    return EXIT_SUCCESS;
}

void println(std::string s) {
    std::cout << s << std::endl;
}

void startTest() {
    println("===== Testing =================");
}

void endTest() {
    println("===== End =====================");
}