#include "Factory.h"
#include "Tile.h"
#include "TileBag.h"

#include <iostream>
#include <string>
#include <memory>
#include <stdexcept>

void printTileBag(TileBag* tilebag);

int main(void){

    int fails = 0;

    std::cout << " ==== RUNNING TESTS ==== " << std::endl;

    try {
        //methods
        std::cout << " ==== Test One - Factory ==== " << std::endl;
        Factory* factory = new Factory();
        std::shared_ptr<Tile> tileR = std::make_shared<Tile>(RED);
        std::shared_ptr<Tile> tileB = std::make_shared<Tile>(DARK_BLUE);
        factory->addTileBack(tileR);
        factory->addTile(tileB, 1);
        delete factory;

    } catch (std::string& error){
        fails++;
        std::cerr << "Caught error: " << error << std::endl;
    }

    try {
        //methods
        std::cout << " ==== Test Two - Factory ==== " << std::endl;
        Factory* factory = new Factory();
        std::shared_ptr<Tile> tileR = std::make_shared<Tile>(RED);
        std::shared_ptr<Tile> tileB = std::make_shared<Tile>(DARK_BLUE);
        factory->addTileBack(tileR);
        factory->addTile(tileB, 1);
        std::shared_ptr<Tile> tileget = factory->getTile(1);
        std::cout << "Tile: " << tileget->toChar() << std::endl;
        std::cout << "Tile: " << factory->getTile(0)->toChar() << std::endl;
        delete factory;

    } catch (std::string& error){
        fails++;
        std::cerr << "Caught error: " << error << std::endl;
    }

    // Test: 
    /* 
    */
    try {
        //methods
        std::cout << " ==== Test Three - TileBag ==== " << std::endl;
        TileBag* tilebag = new TileBag();
        std::shared_ptr<Tile> tileR = std::make_shared<Tile>(RED);
        std::shared_ptr<Tile> tileB = std::make_shared<Tile>(DARK_BLUE);
        std::shared_ptr<Tile> tileY = std::make_shared<Tile>(BLACK);
        tilebag->addBack(tileR);
        tilebag->addFront(tileY);
        tilebag->addFront(tileB);
        printTileBag(tilebag);
        tilebag->removeBack();
        printTileBag(tilebag);
        tilebag->removeFront();
        printTileBag(tilebag);
        delete tilebag;

    } catch (std::string& error){
        fails++;
        std::cerr << "Caught error: " << error << std::endl;
    }
    
    try {
        //methods
        std::cout << " ==== Test Four - Tile ==== " << std::endl;
        std::shared_ptr<Tile> tile1 = std::make_shared<Tile>(RED);
        std::shared_ptr<Tile> tile2 = std::make_shared<Tile>(DARK_BLUE);
        std::shared_ptr<Tile> tile3 = std::make_shared<Tile>(BLACK);
        std::cout << "Tiles: " << tile1->toChar() << tile2->toChar() << tile3->toChar() << std::endl;
        tile1->setColor(DARK_BLUE);
        tile3->setColor(DARK_BLUE);
        std::cout << "Tiles: " << tile1->toChar() << tile2->toChar() << tile3->toChar() << std::endl;

    } catch (std::string& error){
        fails++;
        std::cerr << "Caught error: " << error << std::endl;
    }


    std::cout << " ====== END TEST ======" << std::endl;
    std::cout << "Fails: " << fails << std::endl;

    return EXIT_SUCCESS;
}

void printTileBag(TileBag* tilebag){
    std::shared_ptr<std::vector<Tile>> vector = tilebag->getVector();
    std::cout << "LinkedList Size: " << tilebag->size() << std::endl;
    std::cout << "Front: ";
    for(unsigned int i=0;i<vector->size();i++){
        std::cout << "[";
        std::cout << vector->at(i).toChar();
        std::cout << "]->";
    }
    std::cout << "End." << std::endl;
}

