#ifndef FACTORY_H
#define FACTORY_H

#include <vector>
#include <memory>

#include "BSTree.h"
#include "Tile.h"

/*Factories are the center tiles where players will pick a colour set
* of tiles from a center piece. This also includes the discarded piles
* which is a pile of tiles that fill up with unwanted colors from a
* center factory.
*/

class Factory {
public: 
    Factory();
    Factory(const Factory& other);
    Factory(Factory&& other);
    ~Factory();

    /**
    * Add a tile into the factory to the Back.
    */
    void addTile(std::shared_ptr<Tile> tile);

    /**
    * Removes all tiles of a given color from the Factory.
    */
    void removeTileByColor(TileColor color);

    /**
    * Get the size of the factory vector.
    */
    unsigned int size();
    
    /**
    * Return tile of a given color. Removes the given color tiles.
    */
    std::vector<std::shared_ptr<Tile>> getColorGroup(TileColor color);

    /**
    * Removes all tiles and deletes.
    */
    void clear();

    /**
     * Returns true if the factory contains the first token.
     * Returns false if the factory does not contain the first token.
     */
    bool containsFirstToken();

    /**
     * Returns true if the factory contains the tile passed in.
     * Returns false if the factory does not contain the tile passed in.
     */
    bool validColorGroup(TileColor color);

    unsigned int countTiles(TileColor color);

    const TilePtrVector getAll() const;

private:

    /**
    * Vector of tiles.
    */
    BSTree factory;

};

#endif //FACTORY_H