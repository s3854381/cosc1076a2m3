#ifndef TILE_H
#define TILE_H

#include <stdexcept>
#include <string>

// Only color tiles are stored in the tile bag. Used in Tile(char).
#define COLOR_CHARS "RYBLU"

enum TileColor {
    RED             = 'R', // Red R
    YELLOW          = 'Y', // Yellow Y
    DARK_BLUE       = 'B', // Dark Blue B
    LIGHT_BLUE      = 'L', // Light Blue L
    BLACK           = 'U', // Black U
    FIRST_TOKEN     = 'F', // First player Token/Tile F
    EMPTY           = '.'  // Empty space .
};

class Tile{
public:
    Tile(TileColor color);
    Tile(char color);
    Tile(const Tile& other);
    Tile(Tile&& other);
    ~Tile();
    

    /**
    * Return tiles TileColor enum value.
    */
    TileColor getTile();

    /**
    * Converts and returns the tiles color as a char type.
    */
    char toChar() const;

    /**
    * Sets a Tile to a certain color enum value.
    */
    void setColor(TileColor color);

    bool operator==(const Tile& rhs) const;
    bool operator< (const Tile& rhs) const;
    bool operator!=(const Tile& rhs) const;
    bool operator> (const Tile& rhs) const;
    bool operator<=(const Tile& rhs) const;
    bool operator>=(const Tile& rhs) const;

private:
    TileColor color;

};

#endif //TILE_H