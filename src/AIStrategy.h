#ifndef COSC_1076_ASSIGN2M3_AISTRATEGY_H
#define COSC_1076_ASSIGN2M3_AISTRATEGY_H

#include <limits>
#include <memory>
#include <unordered_map>
#include <vector>

#include "Factory.h"
#include "Player.h"
#include "Turn.h"

/**
 * Code structuring class. Contains most of the actual heuristic logic.
 */
class CountWantedVector {
public:
    /**
     * Vector of the counts of available spaces for this vector's color.
     * If any line is started but not finished for the color, all other counts
     * will be 0.
     */
    std::vector<unsigned int> lineCounts;

    /**
     * Get the maximum number stored in this vector.
     */
    unsigned int getMaxCount();

    /**
     * Return the best line index given that at least one non-first tile will be broken.
     * Precondition: count is greater than at least one number in lineCounts
     * Precondition: count is not 0
     */
    unsigned int getBrokenFit(unsigned int count);

    /**
     * Return the best fit for a given count of tiles, assuming that no non-first tiles
     * will be broken.
     * Precondition: count is less than at least one number in lineCounts
     * Precondition: count is not 0
     */
    unsigned int getUnbrokenFit(unsigned int count);
};

/**
 * AI to play azul. Give it a player with setPlayer, then use getTurn() to retrieve its next turn. 
 */
class AIStrategy {
public:
    AIStrategy(std::vector<std::shared_ptr<Factory>> factories);
    AIStrategy(const AIStrategy& other);
    AIStrategy(AIStrategy&& other);
    ~AIStrategy();

    /**
     * Sets the player for this AI. Required for getTurn to work.
     */
    void setPlayer(std::shared_ptr<Player> player);

    /**
     * Gets this AI's next turn based on its board and the factories.
     * Precondition: setPlayer() has been called. 
     */
    std::shared_ptr<Turn> getTurn();

    /**
     * Returns the name of this AI.
     */
    std::string getName();

private:
    std::vector<std::shared_ptr<Factory>> factories;
    std::shared_ptr<Player> player;

    /**
     * List of all colors. Should ideally be in the Tile class, but this code duplication hasn't
     * been refactored.
     */
    std::vector<TileColor> allColors;

    /**
     * Stores the number of Tiles the AI wants on each line. These are mapped to the tile colors
     * so the AI can decide on turns on a color-by-color basis.
     */
    std::unordered_map<TileColor, CountWantedVector> countVectors;

    /**
     * Updates the classes' CountWantedVectors
     */
    void updateCountVectors();
};

#endif // COSC_1076_ASSIGN2M3_AISTRATEGY_H
