#include "TileBag.h"

TileBag::TileBag(){
    tilebag = std::make_shared<LinkedList<Tile>>();
}

TileBag::TileBag(const std::vector<Tile>& tileVector) {
    tilebag = std::make_shared<LinkedList<Tile>>();
    for (const Tile& tile : tileVector) {
        addBack(std::make_shared<Tile>(tile));
    }
}

TileBag::TileBag(const TileBag& other){
    tilebag = std::make_shared<LinkedList<Tile>>(*other.tilebag);
}

TileBag::TileBag(TileBag&& other){
    tilebag = other.tilebag;
}

TileBag::~TileBag(){
    tilebag = nullptr;
}

unsigned int TileBag::size(){
    return tilebag->size();
}

std::shared_ptr<Tile> TileBag::popFront() {
    std::shared_ptr<Tile> returnValue = tilebag->get(0);
    tilebag->removeFront();
    return returnValue;
}

void TileBag::addBack(std::shared_ptr<Tile> tile){
    return tilebag->addBack(tile);
}

void TileBag::clear(){
    return tilebag->clear();
}

std::shared_ptr<std::vector<Tile>> TileBag::getVector() const {
    return tilebag->getVector();
}