#include "Turn.h"

Turn::Turn(unsigned int factory, char tileChar, unsigned int playerLine) :
    factory(factory),
    tileChar(tileChar),
    playerLine(playerLine)
{}

Turn::Turn(const Turn& other) :
    factory(other.factory),
    tileChar(other.tileChar),
    playerLine(other.playerLine)
{}

Turn::Turn(Turn&& other) :
    factory(other.factory),
    tileChar(other.tileChar),
    playerLine(other.playerLine)
{}

Turn::~Turn() {
    // No memory needs to be deallocated.
}

unsigned int Turn::getFactory() const {
    return factory;
}

char Turn::getTileChar() const {
    return tileChar;
}

unsigned int Turn::getPlayerLine() const {
    return playerLine;
}
