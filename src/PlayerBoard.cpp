#include "PlayerBoard.h"
#include <iostream>

PlayerBoard::PlayerBoard() {
    resetPatternLines();
    resetPlayerMosaic();
    initialiseTemplate('s');
}

PlayerBoard::PlayerBoard(char templateType) {
    resetPatternLines();
    resetPlayerMosaic();
    initialiseTemplate(templateType);
}

PlayerBoard::PlayerBoard(const PlayerBoard& other) {
    for (long unsigned int i = 0; i < other.floorLine.size(); i++) {
        floorLine.push_back(other.floorLine[i]);
    }

    for (int i = 0; i < MOSAIC_ROWS; i++) {
        for (int j = 0; j < MOSAIC_COLUMNS; j++) {
            playerMosaic[i][j].reset(new Tile(*(other.playerMosaic[i][j])));
            mosaicTemplate[i][j].reset(new Tile(*(other.mosaicTemplate[i][j])));
        }
    }

    for (int i = 0; i < PATTERN_LINES_ROWS; i++) {
        for (long unsigned int j = 0; j < other.patternLines[i].size(); j++) {
            patternLines[i].push_back(std::shared_ptr<Tile>(other.patternLines[i][j]));
        }
    }
}

PlayerBoard::PlayerBoard(PlayerBoard&& other) {
    for (long unsigned int i = 0; i < other.floorLine.size(); i++) {
        floorLine.push_back(other.floorLine[i]);
    }

    for (int i = 0; i < MOSAIC_ROWS; i++) {
        for (int j = 0; j < MOSAIC_COLUMNS; j++) {
            playerMosaic[i][j].reset(new Tile(*(other.playerMosaic[i][j])));
            mosaicTemplate[i][j].reset(new Tile(*(other.mosaicTemplate[i][j])));
        }
    }

    for (int i = 0; i < PATTERN_LINES_ROWS; i++) {
        for (long unsigned int j = 0; j < other.patternLines[i].size(); j++) {
            patternLines[i].push_back(std::shared_ptr<Tile>(other.patternLines[i][j]));
        }
    }
}

PlayerBoard::~PlayerBoard() {

}

void PlayerBoard::initialiseTemplate(char templateType) {
    if (templateType == 'b') {
        for (int i = 0; i < MOSAIC_ROWS; ++i) {
            for (int j = 0; j < MOSAIC_COLUMNS; ++j) {
                mosaicTemplate[i][j].reset(new Tile(EMPTY));
            }
        }
    }
    else if (templateType == 's') {
        int colour[MOSAIC_ROWS] = {0,1,2,3,4};

        for (int i = 0; i < MOSAIC_ROWS; ++i) {
            mosaicTemplate[i][colour[0]].reset(new Tile(DARK_BLUE));
            mosaicTemplate[i][colour[1]].reset(new Tile(YELLOW));
            mosaicTemplate[i][colour[2]].reset(new Tile(RED));
            mosaicTemplate[i][colour[3]].reset(new Tile(BLACK));
            mosaicTemplate[i][colour[4]].reset(new Tile(LIGHT_BLUE));

            for (int j = 0; j < MOSAIC_ROWS; ++j) {
                colour[j] = (colour[j] + 1) % 5;
            }
        }
    }
}

void PlayerBoard::resetPlayerMosaic() {
    for (int i = 0; i < MOSAIC_ROWS; ++i) {
        for (int j = 0; j < MOSAIC_COLUMNS; ++j) {
            playerMosaic[i][j].reset(new Tile(EMPTY));
        }
    }
}

void PlayerBoard::resetPatternLines() {
    for (int i = 0; i < PATTERN_LINES_ROWS; i++) {
        patternLines[i].clear();
    }
}

std::vector<std::shared_ptr<Tile>> PlayerBoard::getFloorLine() {
    return floorLine;
}

std::array<std::vector<std::shared_ptr<Tile>>, PATTERN_LINES_ROWS> PlayerBoard::getPatternLines() {
    return patternLines;
}

std::array<std::array<std::shared_ptr<Tile>, MOSAIC_COLUMNS>, MOSAIC_ROWS> PlayerBoard::getPlayerMosaic() {
    return playerMosaic;
}

std::array<std::array<std::shared_ptr<Tile>, MOSAIC_COLUMNS>, MOSAIC_ROWS> PlayerBoard::getTemplate() {
    return mosaicTemplate;
}

void PlayerBoard::patternLineAdd(unsigned int row, std::vector<std::shared_ptr<Tile>>& colourGroup) {
    unsigned int freeSpace = freeSpaceInLine(row);
    for (unsigned int i = 0; i < freeSpace && colourGroup.size() > 0; i++) {
        patternLines[row].push_back(colourGroup.back());
        colourGroup.pop_back();
    }

    if (colourGroup.size() > 0) {
        floorLineAdd(colourGroup);
    }
}

void PlayerBoard::floorLineAdd(std::vector<std::shared_ptr<Tile>>& colourGroup) {
    for (unsigned int i = 0; i < colourGroup.size(); i++) {
        floorLine.push_back(colourGroup.back());
        colourGroup.pop_back();
    }
}

bool PlayerBoard::inMosaicRow(TileColor colour, unsigned int row) {
    bool returnValue = false;
    for (long unsigned int i = 0; i < maxSizeAtIndex[row]; i ++) {
        if (colour == playerMosaic[row][i]->getTile()) {
            returnValue = true;
        }
    }
    return returnValue;
}

int PlayerBoard::countUp(int row, int column) {
    int returnValue;
    if (row < 0 || playerMosaic[row][column]->getTile() == EMPTY) {
        returnValue = 0;
    } else {
        returnValue = 1 + countUp(row - 1, column);
    }
    return returnValue;
}

int PlayerBoard::countDown(int row, int column) {
    int returnValue;
    if (row == MOSAIC_ROWS || playerMosaic[row][column]->getTile() == EMPTY) {
        returnValue = 0;
    } else {
        returnValue = 1 + countDown(row + 1, column);
    }
    return returnValue;
}

int PlayerBoard::countLeft(int row, int column) {
    int returnValue;
    if (column < 0 || playerMosaic[row][column]->getTile() == EMPTY) {
        returnValue = 0;
    } else {
        returnValue = 1 + countLeft(row, column - 1);
    }
    return returnValue;
}

int PlayerBoard::countRight(int row, int column) {
    int returnValue;
    if (column == MOSAIC_COLUMNS || playerMosaic[row][column]->getTile() == EMPTY) {
        returnValue = 0;
    } else {
        returnValue = 1 + countLeft(row, column + 1);
    }
    return returnValue;

}

int PlayerBoard::countVertical(int row, int column) {
    return countUp(row - 1, column) + countDown(row + 1, column);
}

int PlayerBoard::countHorizontal(int row, int column) {
    return countLeft(row, column - 1) + countRight(row, column + 1);
}

int PlayerBoard::endOfRound(TileBag& tileBag) {
    int returnValue = 0;
    for (int row = 0; row < PATTERN_LINES_ROWS; row++) {
        std::vector<std::shared_ptr<Tile>>& currentLine = patternLines[row];
        if(currentLine.size() == maxSizeAtIndex[row]){
            int column = tilePosition(currentLine.front()->getTile(), row);
            playerMosaic[row][column] = currentLine.back();
            currentLine.pop_back();
            while (currentLine.size() > 0) {
                tileBag.addBack(currentLine.back());
                currentLine.pop_back();
            }

            int vPoints = countVertical(row, column);
            int hPoints = countHorizontal(row, column);
            returnValue += 1 + vPoints + hPoints;
            if (vPoints > 0 && hPoints > 0) {
                ++returnValue;
            }
        }
    }
    while (floorLine.size() > 0) {
        if (floorLine.back()->getTile() == FIRST_TOKEN) {
            ;
        } else {
            std::shared_ptr<Tile> back = floorLine.back();
            tileBag.addBack(back);
        }
        if (floorLine.size() > 7) {
            floorLine.pop_back();
        } else if (floorLine.size() > 5) {
            floorLine.pop_back();
            returnValue -= 3;
        } else if (floorLine.size() > 2) {
            floorLine.pop_back();
            returnValue -= 2;
        } else {
            floorLine.pop_back();
            returnValue -= 1;
        }
    }
    return returnValue;
}

int PlayerBoard::tilePosition(TileColor colour, int row) {
    int index = 0;
    for (int i = 0; i < MOSAIC_COLUMNS; i++) {
        if (mosaicTemplate[row][i]->getTile() == colour) {
            index = i;
        }
    }
    return index;
}

bool PlayerBoard::firstTokenCheck() {
    bool returnValue = false;
    for (long unsigned int i = 0; i < floorLine.size(); i++) {
        if(floorLine[i]->getTile() == FIRST_TOKEN) {
            returnValue = true;
        }
    }
    return returnValue;
}

bool PlayerBoard::validPatternLine(unsigned int row, TileColor color) {
    bool validRow = true;
    
    if (patternLines[row].size() > 0) {
        if(patternLines[row].front()->getTile() != color) {
            validRow = false;
        }
        else if (patternLines[row].size() == maxSizeAtIndex[row]) {
            validRow = false;
        }
    }
    if (inMosaicRow(color, row)) {
        validRow = false;
    }

    return validRow;
}

unsigned int PlayerBoard::freeSpaceInLine(unsigned int row) {
    return maxSizeAtIndex[row] - patternLines[row].size();
}

bool PlayerBoard::patternLineStarted(unsigned int row) {
    return freeSpaceInLine(row) != maxSizeAtIndex[row];
}