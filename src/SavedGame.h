#ifndef COSC1076_ASSIGN2_SAVEDGAME_H
#define COSC1076_ASSIGN2_SAVEDGAME_H

#include <memory>
#include <vector>

#include "TileBag.h"
#include "Turn.h"

typedef std::vector<std::string>    StringVector;
typedef std::vector<Tile>           TileVector;
typedef std::vector<Turn>           TurnVector;

/**
 * The conditions required to replay a game up until all added turns have been played.
 * Each time a turn is played, it should be added here.
 * 
 * Understands what data from the game needs to be saved/loaded to/from files.
 */
class SavedGame {
public:

    /**
     * Create a new SavedGame from a new Azul game.
     */
    SavedGame(const TileBag& initialTileBag);
    SavedGame(std::shared_ptr<TileVector> initialTileBag);
    SavedGame(const SavedGame& other);
    SavedGame(SavedGame&& other);
    ~SavedGame();

    /**
     * Adds a player to the SavedGame.
     */
    void addPlayer(std::string playerName);
    /**
     * Add a turn input to the SavedGame using each turn parameter.
     */
    void addTurn(int factoryNum, char tileChar, int playerLine);
    /**
     * Add a turn input to the SavedGame using the Turn class.
     */
    void addTurn(Turn turn);
    /**
     * Get the starting tile bag as store in the saved game.
     */
    const TileVector& getInitialTileBag() const;
    /**
     * Get the names of the players inside the saved game.
     */
    const StringVector& getPlayerNames() const;
    /**
     * Get the turns inside the saved game.
     */
    const TurnVector& getTurns() const;

private:
    /**
     * The tile bag at the initial start of the game as a TileVector.
     */
    std::shared_ptr<TileVector> initialTileBag;
    
    /**
     * The names of the players in the game, starting with who went first.
     */
    std::shared_ptr<StringVector> players;

    /**
     * All of the turns in the game so far.
     */
    std::shared_ptr<TurnVector> turns;
};

#endif // COSC1076_ASSIGN2_SAVEDGAME_H