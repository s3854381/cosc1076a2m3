#ifndef TEST_H
#define TEST_H

/**
* Runs the test mode by running a saved game file, then parsing that saved game to 
* GameEngine to perform start game based on a specified file path.
*/
void testGame(std::string filePath);

#endif