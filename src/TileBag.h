#ifndef TILEBAG_H
#define TILEBAG_H

#include "Tile.h"
#include "LinkedList.h"
#include <memory>

//implemented as a linked list
class TileBag{
public:
    TileBag();
    TileBag(const std::vector<Tile>& tileVector);
    TileBag(const TileBag& other);
    TileBag(TileBag&& other);
    ~TileBag();

    /**
    * Return the current size of the TileBag.
    */
    unsigned int size();

    /**
     * Removes and returns the front tile of the TileBag.
     */
    std::shared_ptr<Tile> popFront();

    /**
    * Add the Tile to the back of the TileBag
    */
    void addBack(std::shared_ptr<Tile> tile);

    /**
    * Removes all Tiles from the TileBag
    */
    void clear();

    /**
    * Returns a pointer to an ordered vector of deep copies of all Tiles in the Tilebag.
    */
    std::shared_ptr<std::vector<Tile>> getVector() const;

private:

    std::shared_ptr<LinkedList<Tile>> tilebag;
    
};

#endif //TILEBAG_H