#include "LinkedList.h"

#include <exception>
#include <iostream>
#include <fstream>
#include <limits>

Node::Node(int value, Node* next) :
    value(value),
    next(next)
{}


LinkedList::LinkedList() {
    head = nullptr;
    tail = nullptr;
    length = 0;
}


LinkedList::LinkedList(const LinkedList& other) {
    head = nullptr;
    tail = nullptr;
    length = 0;
    Node* current = other.head;
    while (current != nullptr) {
        addBack(current->value);
        current = current->next;
    }
}


LinkedList::~LinkedList() {
    clear();
}


unsigned int LinkedList::size() const {
    return length;
}


int LinkedList::get(const unsigned int index) const {
    int returnValue = 0;

    if (index < size()) {
        Node* current = head;
        for(unsigned int i = 0; i != index; ++i) {
            current = current->next;
        }
        returnValue = current->value;
    } else {
        throw std::out_of_range("Linked List get - index out of range.");
    }
    return returnValue;
}

// Error here: adding front to a fresh linked list. throws invalid pointer
// core dumped.
void LinkedList::addFront(int value) {
    head = new Node(value, head);
    if (size() == 0) {
        tail = head;
    }
    ++length;
}


void LinkedList::addBack(int value) {
    if (size() == 0) {
        head = new Node(value, nullptr);
        tail = head;
    } else {
        tail->next = new Node(value, nullptr);
        tail = tail->next;
    }
    ++length;
}


void LinkedList::removeBack() {
    //need a pointer to the current node and another to the previous node.
    Node* current = head;
    //Node* prev = nullptr;
    // Length 0
    if (current != nullptr) {
        // Length 1
        if (current->next == nullptr) {
            removeFront();
        }
        // Length 2+
        else {
            while (current->next->next != nullptr) {
                current = current->next;
            }
            delete current->next;
            current->next = nullptr;
            tail = current;
            --length;
        }
    }
}

//error thrown when there is only one node in the list.
void LinkedList::removeFront() {
    if (size() != 0) {
        Node* current = head->next;
        //Node* toDelete = head;
        delete head;
        head = current;

        --length;
        if (size() == 0) {
            tail = nullptr;
        }
    }
}

void LinkedList::clear() {
    while (size() != 0) {
        removeFront();
    }
}

