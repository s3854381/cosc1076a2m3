#ifndef COSC1076_ASSIGN2_BSTREE_H
#define COSC1076_ASSIGN2_BSTREE_H

#include <exception>
#include <iostream>
#include <memory>
#include <vector>



class BSNode;
typedef std::shared_ptr<BSNode>     BSNodePtr;
typedef std::shared_ptr<int>       intPtr;
typedef std::vector<intPtr>        intPtrVector;

class BSNode {
public:
    BSNode(intPtr data);
    BSNode(const BSNode& other);

    intPtr data;
    BSNodePtr left = nullptr;
    BSNodePtr right = nullptr;

    std::string toString();
};

class BSTree {
public:
    BSTree();
    BSTree(const BSTree& other);
    BSTree(BSTree&& other);
    ~BSTree();

    /**
     * Returns the total number of elements in the tree, by summing the counts of the nodes.
     */
    unsigned int size() const;

    /**
     * Returns the number of items identical to what is pointed to by reference, that are
     * stored in the tree.
     */
    unsigned int count(const int& data) const;

    /**
     * Inserts a int into the tree.
     */
    void insert(intPtr dataPtr);

    intPtr extract(const int& data);

    /**
     * Removes all items identical to dataPtr and returns a vector of pointers to those ints.
     */
    intPtrVector removeAll(const int& data);

    /**
     * Deletes all items in the tree.
     */
    void clear();

    /**
     * Gets a const vector of pointers to all items in the BSTree.
     */
    const intPtrVector getAll() const;

    /**
     * Removes all items in the BSTree and returns a vector of pointers to those ints.
     */
    intPtrVector removeAll();

    void print();

    bool validate();


private:
    BSNodePtr root;
    unsigned int length;
    bool LEFT_CHILD = false;
    bool RIGHT_CHILD = false;

    unsigned int count(const int& data, BSNodePtr node) const;
    BSNodePtr insert(intPtr dataPtr, BSNodePtr node);
    intPtr extract(const int& data, BSNodePtr& node);
    intPtr extractNode(BSNodePtr& node);
    bool validate(BSNodePtr node, intPtr min, intPtr max);
    const intPtrVector getAll(BSNodePtr node) const;

    void print(BSNodePtr node, std::string chain);
};

#endif //COSC1076_ASSIGN2_BSTREE_H