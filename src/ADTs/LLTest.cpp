#include "LinkedList.h"
#include <iostream>

/*
*   Test and Review of LinkedList Implementation
*   Possible Test Cases:
*   1: Test Add front.
    2: Test Add back.
    3: Test Add front.
    4: Test get front.
    5: Test remove front
    6: Test remove back
    7: Test get back.
    8: Test copyconstructor (deep or shallow)
    3: Test clear() on empty linkedlist
    4: Test clear() on linkedlist.
*
*
*/

void printLinkedList(LinkedList* list);

int main(void) {

    //construct a linkedlist.
    //Test Case 1:
    std::cout << "===== Testing =================" << std::endl;
    LinkedList* testOne = new LinkedList();
    testOne->addFront(1);
    testOne->addFront(2);
    testOne->addFront(3);
    testOne->addFront(4);
    printLinkedList(testOne);
    delete testOne;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    LinkedList* testTwo = new LinkedList();
    testTwo->addBack(1);
    testTwo->addBack(2);
    testTwo->addBack(3);
    printLinkedList(testTwo);
    delete testTwo;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    LinkedList* testThree = new LinkedList();
    testThree->addBack(1);
    testThree->addFront(2);
    testThree->addFront(3);
    testThree->addBack(4);
    testThree->addBack(5);
    testThree->addFront(6);
    testThree->addBack(7);
    testThree->addBack(8);
    printLinkedList(testThree);
    delete testThree;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    LinkedList* testFour = new LinkedList();
    testFour->addBack(1);
    printLinkedList(testFour);
    //delete testFour;
    testFour->addFront(2);
    printLinkedList(testFour);
    delete testFour;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    LinkedList* testFive = new LinkedList();
    testFive->addFront(2);
    printLinkedList(testFive);
    testFive->removeFront();
    printLinkedList(testFive);
    delete testFive;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    LinkedList* testSix = new LinkedList();
    delete testSix;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    LinkedList* testSeven = new LinkedList();
    testSeven->addBack(1);
    testSeven->addFront(2);
    testSeven->addFront(3);
    testSeven->addBack(4);
    testSeven->removeFront();
    testSeven->removeBack();
    printLinkedList(testSeven);
    delete testSeven;
    std::cout << "===== End =====================" << std::endl;

     std::cout << "===== Testing =================" << std::endl;
    LinkedList* testEight = new LinkedList();
    testEight->addBack(1);
    testEight->addFront(2);
    testEight->addFront(3);
    testEight->addBack(4);
    printLinkedList(testEight);
    testEight->clear();
    printLinkedList(testEight);
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    LinkedList* testNine = new LinkedList();
    printLinkedList(testNine);
    testNine->clear();
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    LinkedList* testTen = new LinkedList();
    testTen->removeFront();
    printLinkedList(testTen);
    delete testTen;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    LinkedList* test11 = new LinkedList();
    //testTen->get(0);
    printLinkedList(test11);
    delete test11;
    std::cout << "===== End =====================" << std::endl;



    return EXIT_SUCCESS;
}

void printLinkedList(LinkedList* list){
    std::cout << "LinkedList Size: " << list->size() << std::endl;
    std::cout << "Front: ";
    for(unsigned int i=0;i<list->size();i++){
        std::cout << "[";
        std::cout << list->get(i);
        std::cout << "]->";
    }
    std::cout << "End." << std::endl;
}