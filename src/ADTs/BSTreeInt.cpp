#include "BSTreeInt.h"

BSNode::BSNode(intPtr data) :
    data(data),
    left(nullptr),
    right(nullptr)
{}

BSNode::BSNode(const BSNode& other) {

    data.reset(new int(*other.data));

    if (other.left != nullptr) {
        left.reset(new BSNode(*other.left));
    } else {
        left = nullptr;
    }
    if (other.right != nullptr) {
        right.reset(new BSNode(*other.right));
    } else {
        right = nullptr;
    }
}

BSTree::BSTree() {
    root = nullptr;
    length = 0;
}

BSTree::BSTree(const BSTree& other) {
// TODO
}

BSTree::BSTree(BSTree&& other) {
// TODO
}

BSTree::~BSTree() {
    clear();
}

unsigned int BSTree::size() const {
    return length;
}

unsigned int BSTree::count(const int& data) const {
    unsigned int returnValue = 0;
    if (root != nullptr) {
        returnValue = count(data, root);
    }
    return returnValue;
}

unsigned int BSTree::count(const int& data, BSNodePtr node) const {
    unsigned int returnValue = 0;
    
    if (node != nullptr) {
        int nodeData = *node->data;

        if (data < nodeData) {
            returnValue += count(data, node->left);

        } else {
            returnValue += count(data, node->right);
            if (data == nodeData) {
                ++returnValue;
            }
        }
    }
    return returnValue;
}

void BSTree::insert(intPtr dataPtr) {
    root = insert(dataPtr, root);
}

BSNodePtr BSTree::insert(intPtr dataPtr, BSNodePtr node) {

    if (node == nullptr) {
        node.reset(new BSNode(dataPtr));
        ++length;

    } else {
        int data = *dataPtr;
        int nodeData = *node->data;
        if (data < nodeData) {
            node->left = insert(dataPtr, node->left);
            
        } else {
            node->right = insert(dataPtr, node->right);
        }
    }
    return node;
}

intPtr BSTree::extract(const int& data) {

    intPtr returnValue = nullptr;
    if (root != nullptr) {
        int rootData = *root->data;
        if (data < rootData) {
            returnValue = extract(data, root->left);
        } else if (data > rootData) {
            returnValue = extract(data, root->right);
        } else {
            // extract the root
            returnValue = extractNode(root);
        }
    }
    return returnValue;
}

intPtr BSTree::extract(const int& data, BSNodePtr& node) {
    intPtr returnValue = nullptr;

    if (node != nullptr) {
        int nodeData = *node->data;
        if (data < nodeData) {
            returnValue = extract(data, node->left);
        } else if (data > nodeData) {
            returnValue = extract(data, node->right);
        } else {
            returnValue = extractNode(node);
        }
    }

    return returnValue;
}

intPtr BSTree::extractNode(BSNodePtr& node) {
    BSNodePtr nodeL = node->left;
    BSNodePtr nodeR = node->right;
    intPtr returnValue = node->data;

    if (nodeR == nullptr) {
        node = nodeL;
        --length;
    } else {

        if (node->right->left == nullptr) {
            nodeR->left = nodeL;
            node = nodeR;
            --length;
        } else {

            BSNodePtr current = node->right;
            while (current->left->left != nullptr) {
                current = current->left;
            }
            BSNodePtr target = current->left;
            
            current->left = target->right;
            node.reset(new BSNode(extractNode(target)));
            node->right = nodeR;
            node->left = nodeL;
        }
    }

    return returnValue;
}

void BSTree::clear() {
    root = nullptr;
    length = 0;
}

void BSTree::print() {
    print(root, "");
}

void BSTree::print(BSNodePtr node, std::string chain) {
    
    if (node == nullptr) {
        std::cout << chain << "nullptr" << std::endl;
    } else {
        chain += std::to_string(*node->data);
        
        print(node->left, chain + " L> ");
        std::cout << chain << std::endl;
        print(node->right, chain + " R> ");
    }
}

bool BSTree::validate() {
    return validate(root, nullptr, nullptr);
}

bool BSTree::validate(BSNodePtr node, intPtr min, intPtr max) {
    bool returnValue = true;

    if (node == nullptr) {
        return true;
    } else {
        intPtr nodeData = node->data;
        returnValue =
            (min == nullptr || *nodeData >= *min) &&
            (max == nullptr || *nodeData < *max) &&
            validate(node->left, min, nodeData) &&
            validate(node->right, nodeData, max);
    }
    return returnValue;
}

const intPtrVector BSTree::getAll() const {
    return getAll(root);
}

const intPtrVector BSTree::getAll(BSNodePtr node) const {
    intPtrVector returnValue;

    if (node != nullptr) {
        intPtrVector leftTree = getAll(node->left);
        intPtrVector rightTree = getAll(node->right);
        returnValue.insert(returnValue.begin(), leftTree.begin(), leftTree.end());
        returnValue.push_back(node->data);
        returnValue.insert(returnValue.end(), rightTree.begin(), rightTree.end());
    }

    return returnValue;
}