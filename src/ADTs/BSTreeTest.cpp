#include <iostream>

#include "BSTreeInt.h"

int main(void) {
    
    BSTree tree = BSTree();

    if (!tree.validate()) {
        throw new std::invalid_argument("BAD TREE");
    }

    intPtr i1 = std::make_shared<int>(1);
    intPtr i2 = std::make_shared<int>(2);
    intPtr i3 = std::make_shared<int>(3);
    intPtr i4 = std::make_shared<int>(4);
    intPtr i5 = std::make_shared<int>(5);
    intPtr i6 = std::make_shared<int>(6);
    intPtr i7 = std::make_shared<int>(7);
    intPtr i8 = std::make_shared<int>(8);
    intPtr i9 = std::make_shared<int>(9);
    intPtr i10 = std::make_shared<int>(10);
    intPtr i11 = std::make_shared<int>(11);
    intPtr i12 = std::make_shared<int>(12);
    intPtr i13 = std::make_shared<int>(13);
    intPtr i14 = std::make_shared<int>(14);
    intPtr i15 = std::make_shared<int>(15);
    intPtr i16 = std::make_shared<int>(16);
    intPtr i17 = std::make_shared<int>(17);
    intPtr i18 = std::make_shared<int>(18);
    intPtr i19 = std::make_shared<int>(19);
    intPtr i20 = std::make_shared<int>(20);
    intPtr i21 = std::make_shared<int>(21);
    intPtr i22 = std::make_shared<int>(22);
    intPtr i23 = std::make_shared<int>(23);
    intPtr i24 = std::make_shared<int>(24);
    intPtr i25 = std::make_shared<int>(25);
    intPtr i26 = std::make_shared<int>(26);
    intPtr i27 = std::make_shared<int>(27);
    intPtr i28 = std::make_shared<int>(28);
    intPtr i29 = std::make_shared<int>(29);
    intPtr i30 = std::make_shared<int>(30);
    intPtr i31 = std::make_shared<int>(31);



    tree.insert(i16);

    tree.insert(i8);
    tree.insert(i24);

    tree.insert(i4);
    tree.insert(i12);
    tree.insert(i20);
    tree.insert(i28);

    tree.insert(i2);
    tree.insert(i6);
    tree.insert(i10);
    tree.insert(i14);
    tree.insert(i18);
    tree.insert(i22);
    tree.insert(i26);
    tree.insert(i30);

    
    tree.insert(i1);
    tree.insert(i3);
    tree.insert(i5);
    tree.insert(i7);
    tree.insert(i9);
    tree.insert(i11);
    tree.insert(i13);
    tree.insert(i15);
    tree.insert(i17);
    tree.insert(i19);
    tree.insert(i21);
    tree.insert(i23);
    tree.insert(i25);
    tree.insert(i27);
    tree.insert(i29);
    tree.insert(i31);


    if (!tree.validate()) {
        throw new std::invalid_argument("BAD TREE");
    }

    tree.extract(16);


    for(const intPtr& i : tree.getAll()) {
        std::cout << *i << std::endl;
    }

    if (!tree.validate()) {
        throw new std::invalid_argument("BAD TREE");
    }

    tree.print();

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    tree.clear();

    
    if (!tree.validate()) {
        throw new std::invalid_argument("BAD TREE");
    }

    tree.insert(std::make_shared<int>(100));
    tree.insert(std::make_shared<int>(0));
    tree.insert(std::make_shared<int>(-1));
    tree.insert(std::make_shared<int>(99));
    tree.insert(std::make_shared<int>(10));
    tree.insert(std::make_shared<int>(98));
    tree.insert(std::make_shared<int>(5));
    tree.insert(std::make_shared<int>(20));
    tree.insert(std::make_shared<int>(15));
    tree.insert(std::make_shared<int>(97));
    tree.insert(std::make_shared<int>(30));
    tree.insert(std::make_shared<int>(25));
    tree.insert(std::make_shared<int>(96));
    tree.insert(std::make_shared<int>(40));
    tree.insert(std::make_shared<int>(35));
    tree.insert(std::make_shared<int>(95));
    tree.insert(std::make_shared<int>(50));
    tree.insert(std::make_shared<int>(45));

    tree.insert(std::make_shared<int>(20));
    tree.insert(std::make_shared<int>(20));
    tree.insert(std::make_shared<int>(20));

    
    if (!tree.validate()) {
        throw new std::invalid_argument("BAD TREE");
    }

    tree.print();

    

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    tree.extract(20);

    
    if (!tree.validate()) {
        throw new std::invalid_argument("BAD TREE");
    }

    tree.print();

    std::cout << std::endl;

    std::cout << "Num of 20s: " << tree.count(20) << std::endl;

    
    if (!tree.validate()) {
        throw new std::invalid_argument("BAD TREE");
    }

    std::cout << std::endl;

    for(const intPtr& i : tree.getAll()) {
        std::cout << *i << std::endl;
    }
}