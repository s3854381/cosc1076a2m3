#ifndef LINKED_LIST_H
#define LINKED_LIST_H 

class Node {
public:
    Node(int value, Node* next);

    int value;
    Node* next;
};

/**
 * int implementation of a single-linked list
 * 
 * Needs to be thoroughly tested
 * 
 * Stores a pointer to the list tail, and stores the size of the list.
 * Because of the tail pointer, addBack is O(1), however removeBack is
 * still O(n)
 * 
 * 
 * Author: Michael Asquith
 * Date: 2020-09-09
 */ 
class LinkedList {
public:
    LinkedList();
    LinkedList(const LinkedList& other);
    ~LinkedList();

/**
 * Return the current size of the Linked List.
 */
    unsigned int size() const;

/**
 * Get the value at the given index.
 */
    int get(const unsigned int index) const;

/**
 * Add the value to the back of the Linked List
 */
    void addBack(int value);

/**
 * Add the value to the front of the Linked List
 */
    void addFront(int value);

/**
 * Remove the value at the back of the Linked List
 */
    void removeBack();

/**
* Remove the value at the front of the Linked List
*/
    void removeFront();

/**
* Removes all values from the Linked List
*/
    void clear();

private:

    Node* head;
    Node* tail;
    unsigned int length;
};

#endif // LINKED_LIST_H
