#include <iostream>

#include "LinkedListTemplate.h"

/*
*   Test and Review of LinkedList Implementation
*   Possible Test Cases:
*   1: Test Add front.
    2: Test Add back.
    3: Test Add front.
    4: Test get front.
    5: Test remove front
    6: Test remove back
    7: Test get back.
    8: Test copyconstructor (deep or shallow)
    3: Test clear() on empty linkedlist
    4: Test clear() on linkedlist.
*
*
*/

// Int class that prints out when its destructor is called.
class myInt {
public:
    int i;
    myInt(int i) {
        this->i = i;
    }
    ~myInt() {
        // std::cout << "myInt::~myInt(" << i << ")" << std::endl;
    }
};

void printLinkedList(std::shared_ptr<LinkedList<myInt>> list);

int main(void) {

    //construct a linkedlist.
    //Test Case 1:
    std::cout << "===== Testing =================" << std::endl;
    std::shared_ptr<LinkedList<myInt>> testOne = std::make_shared<LinkedList<myInt>>();
    testOne->addFront(std::make_shared<myInt>(1));
    testOne->addFront(std::make_shared<myInt>(2));
    testOne->addFront(std::make_shared<myInt>(3));
    testOne->addFront(std::make_shared<myInt>(4));
    std::cout << "EXPECTED: Front: [4]->[3]->[2]->[1]->End." << std::endl;
    printLinkedList(testOne);
    testOne = nullptr;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    std::shared_ptr<LinkedList<myInt>> testTwo = std::make_shared<LinkedList<myInt>>();
    testTwo->addBack(std::make_shared<myInt>(1));
    testTwo->addBack(std::make_shared<myInt>(2));
    testTwo->addBack(std::make_shared<myInt>(3));
    std::cout << "EXPECTED: Front: [1]->[2]->[3]->End." << std::endl;
    printLinkedList(testTwo);
    testTwo = nullptr;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    std::shared_ptr<LinkedList<myInt>> testThree = std::make_shared<LinkedList<myInt>>();
    testThree->addBack(std::make_shared<myInt>(1));
    testThree->addFront(std::make_shared<myInt>(2));
    testThree->addFront(std::make_shared<myInt>(3));
    testThree->addBack(std::make_shared<myInt>(4));
    testThree->addBack(std::make_shared<myInt>(5));
    testThree->addFront(std::make_shared<myInt>(6));
    testThree->addBack(std::make_shared<myInt>(7));
    testThree->addBack(std::make_shared<myInt>(8));
    std::cout << "EXPECTED: Front: [6]->[3]->[2]->[1]->[4]->[5]->[7]->[8]->End." << std::endl;
    printLinkedList(testThree);
    testThree = nullptr;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    std::shared_ptr<LinkedList<myInt>> testFour = std::make_shared<LinkedList<myInt>>();
    testFour->addBack(std::make_shared<myInt>(1));
    std::cout << "EXPECTED: Front: [1]->End." << std::endl;
    printLinkedList(testFour);
    //testFour = nullptr;
    testFour->addFront(std::make_shared<myInt>(2));
    std::cout << "EXPECTED: Front: [2]->[1]->End." << std::endl;
    printLinkedList(testFour);
    testFour = nullptr;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    std::shared_ptr<LinkedList<myInt>> testFive = std::make_shared<LinkedList<myInt>>();
    testFive->addFront(std::make_shared<myInt>(2));
    std::cout << "EXPECTED: Front: [2]->End." << std::endl;
    printLinkedList(testFive);
    testFive->removeFront();
    std::cout << "EXPECTED: Front: End." << std::endl;
    printLinkedList(testFive);
    testFive = nullptr;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    std::shared_ptr<LinkedList<myInt>> testSix = std::make_shared<LinkedList<myInt>>();
    testSix = nullptr;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    std::shared_ptr<LinkedList<myInt>> testSeven = std::make_shared<LinkedList<myInt>>();
    testSeven->addBack(std::make_shared<myInt>(1));
    testSeven->addFront(std::make_shared<myInt>(2));
    testSeven->addFront(std::make_shared<myInt>(3));
    testSeven->addBack(std::make_shared<myInt>(4));
    testSeven->removeFront();
    testSeven->removeBack();
    std::cout << "EXPECTED: Front: [2]->[1]->End." << std::endl;
    printLinkedList(testSeven);
    testSeven = nullptr;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    {
        std::shared_ptr<LinkedList<myInt>> testEight = std::make_shared<LinkedList<myInt>>();
        testEight->addBack(std::make_shared<myInt>(1));
        testEight->addFront(std::make_shared<myInt>(2));
        testEight->addFront(std::make_shared<myInt>(3));
        testEight->addBack(std::make_shared<myInt>(4));
        std::cout << "EXPECTED: Front: [3]->[2]->[1]->[4]->End." << std::endl;
        printLinkedList(testEight);
        testEight->clear();
        std::cout << "EXPECTED: Front: End." << std::endl;
        printLinkedList(testEight);
    }
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    std::shared_ptr<LinkedList<myInt>> testNine = std::make_shared<LinkedList<myInt>>();
    std::cout << "EXPECTED: Front: End." << std::endl;
    printLinkedList(testNine);
    testNine->clear();
    testNine = nullptr;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    std::shared_ptr<LinkedList<myInt>> testTen = std::make_shared<LinkedList<myInt>>();
    testTen->removeFront();
    std::cout << "EXPECTED: Front: End." << std::endl;
    printLinkedList(testTen);
    testTen = nullptr;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    std::shared_ptr<LinkedList<myInt>> test11 = std::make_shared<LinkedList<myInt>>();
    //testTen->get(0);
    std::cout << "EXPECTED: Front: End." << std::endl;
    printLinkedList(test11);
    test11 = nullptr;
    std::cout << "===== End =====================" << std::endl;

    std::cout << "===== Testing =================" << std::endl;
    std::shared_ptr<LinkedList<myInt>> test12 = std::make_shared<LinkedList<myInt>>();
    test12->addBack(std::make_shared<myInt>(1));
    test12->addFront(std::make_shared<myInt>(2));
    test12->addFront(std::make_shared<myInt>(3));
    test12->addBack(std::make_shared<myInt>(4));
    test12->addBack(std::make_shared<myInt>(5));
    test12->addFront(std::make_shared<myInt>(6));
    test12->addBack(std::make_shared<myInt>(7));
    test12->addBack(std::make_shared<myInt>(8));
    std::shared_ptr<std::vector<myInt>> vector12 = test12->getVector();
    std::cout << "EXPECTED: [6,3,2,1,4,5,7,8,]" << std::endl;
    std::cout << "OUTPUT:   [";
    for (const myInt& i : *vector12) {
        std::cout << i.i << ",";
    }
    std::cout << "]" << std::endl;
    test12 = nullptr;
    vector12 = nullptr;
    std::cout << "===== End =====================" << std::endl;

    return EXIT_SUCCESS;
}

void printLinkedList(std::shared_ptr<LinkedList<myInt>> list){
    std::cout << "LinkedList Size: " << list->size() << std::endl;
    std::cout << "OUTPUT:   Front: ";
    for(unsigned int i=0;i<list->size();i++){
        std::cout << "[";
        std::cout << (*(list->get(i))).i;
        std::cout << "]->";
    }
    std::cout << "End." << std::endl;
}