#include "Player.h"
#include <iostream>


Player::Player(std::string name) :
    name(name),
    points(0),
    firstToken(false)
{
    board.reset(new PlayerBoard());
}

Player::Player(std::string name, char templateType) :
    name(name),
    points(0),
    firstToken(false)
{
    board.reset(new PlayerBoard(templateType));
}

Player::Player(const Player& other) :
    name(other.name),
    points(other.points),
    firstToken(other.firstToken)
{
    board.reset(new PlayerBoard(*(other.board)));
}

Player::Player(Player&& other) :
    name(other.name),
    points(other.points),
    firstToken(other.firstToken)
{
    board.reset(new PlayerBoard(*(other.board)));
}

Player::~Player() {

}

std::string Player::getName() {
    return name;
}

int Player::getPoints() {
    return points;
}

void Player::addPoints(int points) {
    this->points += points;
}

std::array<std::array<std::shared_ptr<Tile>, MOSAIC_COLUMNS>, MOSAIC_ROWS> Player::getPlayerMosaic() {
    return board->getPlayerMosaic();
}

std::array<std::array<std::shared_ptr<Tile>, MOSAIC_COLUMNS>, MOSAIC_ROWS> Player::getMosaicTemplate() {
    return board->getTemplate();
}

void Player::patternLineAdd(unsigned int row, std::vector<std::shared_ptr<Tile>>& colourGroup) {
    board->patternLineAdd(row, colourGroup);
}

void Player::floorLineAdd(std::vector<std::shared_ptr<Tile>>& colourGroup) {
    board->floorLineAdd(colourGroup);
}

std::array<std::vector<std::shared_ptr<Tile>>, PATTERN_LINES_ROWS> Player::getPatternLines() {
    return board->getPatternLines();
}

std::vector<std::shared_ptr<Tile>> Player::getFloorLine() {
    return board->getFloorLine();
}

bool Player::checkIfFirst() {
    return firstToken;
}

void Player::addFirstToken(std::vector<std::shared_ptr<Tile>>& colourGroup) {
    board->floorLineAdd(colourGroup);
    firstToken = true;
}

void Player::endOfRound(TileBag& tileBag) {
    points += board->endOfRound(tileBag);
    if (points < 0) {
        points = 0;
    }
}

void Player::resetFirstToken() {
    firstToken = false;
}

bool Player::validPatternLine(unsigned int row, TileColor colour) {
    bool validRow = board->validPatternLine(row, colour);
    if (!validRow) {
        throw std::invalid_argument("Invalid Argument. Selected pattern line is full.");
    }
    return validRow;
}

unsigned int Player::freeSpaceInLine(unsigned int row) {
    return board->freeSpaceInLine(row);
}

bool Player::patternLineStarted(unsigned int row) {
    return board->patternLineStarted(row);
}