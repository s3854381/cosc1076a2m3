#include <iostream>
#include <string>

#include "Test.h"
#include "AzulStdIO.h"
#include "GameEngine.h"

bool menuLogic(bool aiMode);


int main(int argc, char** argv) {
    bool exitStatus = false;
    bool aiMode = false;

    if (argc != 1) {
        std::string option = std::string(argv[1]);

        //Enter into Testing Mode, parse the file into Testing
        if(option == "-t" ) {
            exitStatus = true;
            if(argv[2] == nullptr){
                std::cout << "Err: No save file specified." << std::endl;
            } else {
                testGame(argv[2]);
            }
        } else if (option == "--ai") {
            aiMode = true;

        } else {
            std::cout << option << " is not an available command. " << std::endl;
            std::cout << "Available command(s) are: " << std::endl;
            std::cout << "-t   >>>  Enters into Testing Mode." << std::endl;
            std::cout << "--ai >>>  Enables the AI as player 2." << std::endl;
            exitStatus = true;
        }
    }
    
    while (exitStatus == false) {
            exitStatus = menuLogic(aiMode);
    }
    
    return EXIT_SUCCESS;
}

bool menuLogic(bool aiMode) {
    std::shared_ptr<AzulStdIO> azulStdIO = std::make_shared<AzulStdIO>();
    PromptOption inputValue;
    bool returnValue = false;
    
    inputValue = azulStdIO->getMenuOption();

    if (inputValue == START_GAME) {
        GameEngine engine = GameEngine(aiMode);
        returnValue = engine.startGame(*azulStdIO);
    }
    else if (inputValue == LOAD_GAME) {
        GameEngine engine = GameEngine(aiMode);
        if(engine.loadGame(*azulStdIO)) {
            returnValue = engine.startGame(*azulStdIO);
        }
    }
    else if (inputValue == CREDITS) {
        azulStdIO->printCredits();
        returnValue = false;
    }
    else if (inputValue == QUIT) {
        returnValue = true;
    }
    if (returnValue) {
        azulStdIO->println("Goodbye");
    }
    return returnValue;
}