#ifndef COSC1076_ASSIGN2_AZULSTDIN_H
#define COSC1076_ASSIGN2_AZULSTDIN_H

#include <fstream>
#include <iostream>
#include <memory>
#include <queue>
#include <unordered_map>

#include "SavedGame.h"

#define NUM_PLAYERS 2

enum PromptOption {
    CREDITS,
    LOAD_GAME,
    QUIT,
    SAVE,
    START_GAME,
    STRING,
    TURN
    };

/**
 * Unordered map of strings which are valid user inputs, and PromptOption which
 * are the corresponding code commands. 
 */ 
typedef std::unordered_map<std::string, PromptOption> InputMap;

/**
 * Azul input methods using std::cin. Also includes file reading methods.
 */
class AzulStdIn {
public:
    AzulStdIn();
    AzulStdIn(const AzulStdIn& other);
    AzulStdIn(AzulStdIn&& other);
    ~AzulStdIn();

    /**
     * Author: Michael Asquith
     * Created: 2020-09-19
     * 
     * Prompts standard input for a string matching a key in inputMap. If a match
     * is found, it returns inputMap's value associated with that key.
     * 
     * If the end of file character is read, PromptOption.QUIT is returned.
     * 
     * If no match is found, it throw an std::invalid_argument exception.
     */
    PromptOption promptUser(const InputMap& inputMap);

    /**
     * Gets a string which has been previously stored from the stringPrompt()
     * method.
     */
    std::string getString();

    /**
     * Returns a pointer to a Turn from the information already in standard input.
     * 
     * The stored arguments were retrieved from standard input when the turn option
     * was last in standard input. If the arguments are not of the form <int> <char>
     * <int>, then a std::logic_error will be thrown.
     */
    std::shared_ptr<Turn> getTurn();

    /**
     * Loads a savedGame from a file. The filename is the next string in standard input.
     * 
     * Returns a SavedGame of the selected file. Unknown error handling.
     */
    std::shared_ptr<SavedGame> loadGame(std::string filename);

    /**
     * Returns the number of stored arguments currently stored.
     */
    unsigned int numStoredArgs() { return storedArgs.size(); }

    /**
     * Prompts the user for a string. Returns STRING if a string has been
     * entered, QUIT if eof() is reached, and throws a std::invalid_argument if
     * no string was input.
     */
    PromptOption stringPrompt();

private:

    /**
     * A list of strings which were the last set of arguments to be entered into
     * the console.
     */
    std::queue<std::string> storedArgs;

    /**
     * Clears any arguments stored in the class.
     */
    void clearStoredArgs();

    /**
     * Stores all the arguments in a line from an istream.
     */
    void storeLineAsArgs(std::istream& istream);

    /**
     * Pops the first item from storedArgs and returns it. Undefined
     * behaviour if storedArgs as no elements.
     */
    std::string popStoredArg();
    
    /**
     * Reads the next line if ifstream, assuming that each character is
     * a tile. Returns a vector to those tiles in order.
     */
    std::shared_ptr<TileVector> readTileBag(std::ifstream& ifstream);

    /**
     * Reads the next two lines as player names, and returns those names
     * in a vector in order.
     */
    std::shared_ptr<StringVector> readPlayerNames(std::ifstream& ifstream);

    /**
     * Reads until eof(), assuming each line of ifstream is a Turn of the form:
     *      turn i c j
     * where i is the factory number, c is the tile character, and j is the
     * player line number.
     */
    std::shared_ptr<TurnVector> readTurns(std::ifstream& ifstream);

    /**
     * Calls std::getline, but removes the last character if it is '\\r'.
     */
    void getLine(std::istream& istream, std::string& string);
};

#endif // #ifndef COSC1076_ASSIGN2_AZULSTDIN_H