#include "AzulStdIO.h"

AzulStdIO::AzulStdIO() {
    azulStdIn = std::make_unique<AzulStdIn>();
    azulStdOut = std::make_unique<AzulStdOut>();
}

AzulStdIO::AzulStdIO(const AzulStdIO& other) {
    azulStdIn = std::make_unique<AzulStdIn>(*other.azulStdIn);
    azulStdOut = std::make_unique<AzulStdOut>(*other.azulStdOut);
}

AzulStdIO::AzulStdIO(AzulStdIO&& other) {
    azulStdIn = std::move(other.azulStdIn);
    azulStdOut = std::move(other.azulStdOut);
}

AzulStdIO::~AzulStdIO() {}

std::string AzulStdIO::getString() {
    return azulStdIn->getString();
}

PromptOption AzulStdIO::getTurnOption() {
    return optionPrompt(TURN_OPTIONS);
}

PromptOption AzulStdIO::getMenuOption() {

    azulStdOut->printMenu();
    
    return optionPrompt(MENU_OPTIONS);
}

std::shared_ptr<Turn> AzulStdIO::getTurn() {
    return azulStdIn->getTurn();
}

std::shared_ptr<SavedGame> AzulStdIO::loadGame(std::string filename) {
    return azulStdIn->loadGame(filename);
}

bool AzulStdIO::saveGame(const SavedGame& savedGame) {
    bool success = false;

    try {
        if (azulStdIn->numStoredArgs() != 1) {
            throw std::invalid_argument("Incorrect number of arguments.");
        } else {
            std::string filename = azulStdIn->getString();
            azulStdOut->saveGame(savedGame, filename);
        }
    } catch (std::logic_error& e) {
        throw std::logic_error("Invalid turn arguments.");
    }
    
    return success;
}

void AzulStdIO::println(std::string string) {
    azulStdOut->println(string);
}

void AzulStdIO::println() {
    println("");
}

PromptOption AzulStdIO::stringPrompt() {
    PromptOption returnValue = QUIT;
    bool eof;

    do {
        eof = true;
        printPrompt();

        try {
            returnValue = azulStdIn->stringPrompt();
        } catch (const std::logic_error& e) {
            eof = false;
        }
    } while (!eof);

    if (returnValue == QUIT) {
        println();
    }

    return returnValue;
}

void AzulStdIO::outputTileBag(TileBag& tileBag) {
    azulStdOut->outputTileBag(tileBag);
}

void AzulStdIO::outputFactories(std::vector<std::shared_ptr<Factory>>& factories) {
    azulStdOut->outputFactories(factories);
}

void AzulStdIO::outputPlayerBoard(Player& player) {
    azulStdOut->outputPlayerBoard(player);
}

void AzulStdIO::outputPlayerScore(Player& player) {
    azulStdOut->outputPlayerScore(player);
}

void AzulStdIO::outputEndGame(std::vector<std::shared_ptr<Player>>& players) {
    azulStdOut->outputEndGame(players);
}

void AzulStdIO::printCredits() {
    azulStdOut->printCredits();
}

PromptOption AzulStdIO::optionPrompt(const InputMap& inputMap) {
    PromptOption returnValue = QUIT;
    bool eof;

    do {
        eof = true;
        printPrompt();
        
        try {
            returnValue = azulStdIn->promptUser(inputMap);
        } catch (const std::logic_error& e) {
            println("Invalid Input");
            eof = false;
        }

    } while (!eof);

    if (returnValue == QUIT) {
        println();
    }

    return returnValue;
}

void AzulStdIO::printPrompt() {
    azulStdOut->printPrompt();
}