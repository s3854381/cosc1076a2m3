#include "GameEngine.h"

GameEngine::GameEngine() {
    GameEngine(false);
}

GameEngine::GameEngine(bool aiMode) :
    templateType('s'),
    testMode(false),
    aiMode(aiMode),
    numOfPlayers(2)
{
    tileBag.reset(new TileBag());
    generateTileBag(10);
    savedGame.reset(new SavedGame(*tileBag));
}

//Parse into GameEngine a prebuilt tileBag to do testingMode. Should parse a savedGame in probably- Ronald
GameEngine::GameEngine(int players, std::shared_ptr<SavedGame> save, bool testMode) :
    templateType('s'),
    testMode(testMode),
    numOfPlayers(players)
{
    this->savedGame = save;
    StringVector playerNames = savedGame->getPlayerNames();
    for(std::string& name : playerNames) {
        addPlayer(name);
    }
    if (testMode == true){
        this->tileBag.reset(new TileBag(savedGame->getInitialTileBag()));
    }
}

GameEngine::~GameEngine() {
    players.clear();
    factories.clear();
}

GameEngine::GameEngine(const GameEngine& other) {
    for (long unsigned int i = 0; i < players.size(); i++) {
        players.push_back(other.players[i]);
    }
    savedGame.reset(new SavedGame(*(other.savedGame)));
    for (long unsigned int i = 0; i < factories.size(); i++) {
        factories.push_back(other.factories[i]);
    }
    tileBag.reset(new TileBag(*(other.tileBag)));
    templateType = other.templateType;
}

GameEngine::GameEngine(GameEngine&& other) {
    players = std::move(other.players);
    savedGame.reset(new SavedGame(*(other.savedGame)));
    factories = std::move(other.factories);
    tileBag.reset(new TileBag(*(other.tileBag)));
    templateType = other.templateType;
}

void GameEngine::addPlayer(std::string name) {
    std::shared_ptr<Player> player = std::make_shared<Player>(name, templateType);
    players.push_back(player);
}

bool GameEngine::loadGame(AzulStdIO& azulStdIO) {
    bool success = false;
    std::cout << "Input filename to load from:" << std::endl;

    if (azulStdIO.stringPrompt() == STRING) {
        std::string filename = azulStdIO.getString();
        try {
            savedGame = azulStdIO.loadGame(filename);
            StringVector playerNames = savedGame->getPlayerNames();
            for(std::string& name : playerNames) {
                addPlayer(name);
            }
            this->tileBag.reset(new TileBag(savedGame->getInitialTileBag()));
            loadMode = true;
            success = true;
        } catch (std::exception& e) {
            std::cout << "Failed to load game from " << filename << std::endl << std::endl;   
        }
    }

    return success;
}

bool GameEngine::startGame(AzulStdIO& azulStdIO) {
    
    generateFactories();
    AIStrategy ai = AIStrategy(factories);
    // Call to tell user to enter names here
    bool exit = false;
    for (int i = 0; !exit && !skipStdIO() && i < numOfPlayers; ++i) {
        if (aiMode && i == AI_PLAYER_NUMBER) {
            addPlayer(ai.getName());
        } else {
            std::cout << "Input player " << i + 1 << "'s name." << std::endl;
            if (azulStdIO.stringPrompt() == STRING) {
                std::string playerName = azulStdIO.getString();
                addPlayer(playerName);
                savedGame->addPlayer(playerName);
                std::cout << std::endl;
            } else {
                exit = true;
            }
        }
    }

    if (!exit) {
        if (aiMode) {
            ai.setPlayer(players.at(AI_PLAYER_NUMBER));
        }
        unsigned int turnCount = 0;
        do {
            if(!skipStdIO()){
                std::cout << std::endl;
                std::cout << "=== Start Round ===" << std::endl;
                std::cout << std::endl;
            }
            unsigned int playerIndex = 0;
            if (rounds > 1) {
                for (long unsigned int i = 0; i < players.size(); i++) {
                    if (players[i]->checkIfFirst()) {
                        playerIndex = i;
                        players[i]->resetFirstToken();
                    }
                }
            }
            unsigned int choice;
            char charChoice;
            unsigned int rowChoice;

            while (!factoriesAreEmpty() && !exit) {
                // Output board state and factories to player and ask player to choose a factory
                // Testing output of TileBag and Factories
                if (!skipStdIO()) {
                    std::cout << "TURN FOR PLAYER: " << players[playerIndex]->getName() << std::endl;
                    azulStdIO.outputFactories(factories);
                    azulStdIO.outputPlayerBoard(*players[playerIndex]);
                }

                PromptOption playerOption;

                if(skipStdIO()) {
                    // Iterate through the turns.
                    playerOption = TURN;
                    Turn loadedTurn = savedGame->getTurns().at(turnCount);
                    choice = loadedTurn.getFactory();
                    charChoice = loadedTurn.getTileChar();
                    rowChoice = loadedTurn.getPlayerLine();
                } else if (aiMode && playerIndex == AI_PLAYER_NUMBER) {
                    // AI taking the wheel
                    playerOption = TURN;
                    std::shared_ptr<Turn> aiTurn = ai.getTurn();
                    choice = aiTurn->getFactory();
                    charChoice = aiTurn->getTileChar();
                    rowChoice = aiTurn->getPlayerLine();
                    savedGame->addTurn(choice, charChoice, rowChoice);

                    std::cout << players[playerIndex]->getName() <<" has chosen: turn " << choice << " " << charChoice << " " << rowChoice;
                    std::cout << std::endl << std::endl;

                    
                } else {
                    // Input for choices for a normal game.
                    bool validInput = false;
                    while (!validInput && !exit) {
                        try {
                            playerOption = azulStdIO.getTurnOption();
                            if (playerOption == TURN) {
                                std::shared_ptr<Turn> t = azulStdIO.getTurn();
                                choice = t->getFactory();
                                charChoice = t->getTileChar();
                                rowChoice = t->getPlayerLine();
                                validInput = verifyTurnInputs(choice, charChoice, rowChoice);
                                validInput = validInput && factories[choice]->validColorGroup(charToColour(charChoice));
                                if (rowChoice > 0) {
                                    validInput = validInput && players[playerIndex]->validPatternLine(rowChoice - 1, charToColour(charChoice));
                                }
                                savedGame->addTurn(choice, charChoice, rowChoice);
                            } else if (playerOption == SAVE) {
                                azulStdIO.saveGame(*savedGame);
                            } else if (playerOption == QUIT) {
                                exit = true;
                            }
                        } catch (std::logic_error& e) {
                            std::cout << "Invalid turn input." << std::endl;
                            validInput = false;
                        }
                    }
                    if(validInput){
                        std::cout << "Turn successful. \n" << std::endl;
                    }
                }
                if (playerOption == TURN) {
                    Factory& chosenFactory = *factories[choice];
                    Player& currentPlayer = *players[playerIndex];
                    TileColor chosenColor = charToColour(charChoice);

                    // If the chosenFactory contains the first token, give to that player
                    if (chosenFactory.containsFirstToken()) {
                        std::vector<std::shared_ptr<Tile>> firstToken = chosenFactory.getColorGroup(FIRST_TOKEN);
                        currentPlayer.addFirstToken(firstToken);
                    }

                    // Get the tiles from the chosen factory
                    std::vector<std::shared_ptr<Tile>> tileChoice = chosenFactory.getColorGroup(chosenColor);

                    // If the chosen row is the floor line.
                    if (rowChoice == 0) {
                        currentPlayer.floorLineAdd(tileChoice);
                        // If they weren't taken from the center factory, add the rest of the tiles there.
                        if(choice != 0){
                            addToCenterFactory(choice);
                        } else {
                            // If they were taken from the center factory, remove all tiles of that colour
                            chosenFactory.removeTileByColor(chosenColor);
                        }
                    } else {
                        // If the chosen row isn't the floor line, add it to the player's pattern line
                        currentPlayer.patternLineAdd(rowChoice - 1, tileChoice);
                        // If they weren't taken from the center factory, add the rest of the tiles there.
                        if(choice != 0){
                            addToCenterFactory(choice);
                        } else {
                            // If they were taken from the center factory, remove all tiles of that colour
                            chosenFactory.removeTileByColor(chosenColor);
                        }
                    }

                    turnCount++;
                    goToNextPlayer(playerIndex);
                    
                } else if (playerOption == QUIT) {
                    exit = true;
                }

            // To end test mode prematurely if turns finish before a round ends.
            if (turnCount == savedGame->getTurns().size() && !factoriesAreEmpty()) {
                if(testMode) {
                    exit = true;
                } else if (loadMode) {
                    loadMode = false;
                }
            }

            } // End of the while loop.

            if(!skipStdIO() && !exit){
                std::cout << "=== END OF ROUND ===" << std::endl;
            }
            
            if (!exit) {
                endOfRound();
            }

            if (turnCount == savedGame->getTurns().size()) {
                if(testMode) {
                    azulStdIO.outputFactories(factories);
                    for (long unsigned int i = 0; i < players.size(); i++) {
                        azulStdIO.outputPlayerBoard(*players[i]);
                    }
                    exit = true;
                } else if (loadMode) {
                    loadMode = false;
                }
            }
            rounds++;
        } while (rounds <= MAX_ROUNDS && !exit);

        //Output final game result
        if(rounds > MAX_ROUNDS) {
            azulStdIO.outputEndGame(players);
        }
    }

    return exit;
}

void GameEngine::generateTileBag(int seed) {
    std::default_random_engine engine(seed);
    std::vector<std::shared_ptr<Tile>> tempBag = generateTempBag();
    std::shuffle(std::begin(tempBag), std::end(tempBag), engine);
    while(tempBag.size() > 0) {
        tileBag->addBack(tempBag.back());
        tempBag.pop_back();
    }
}

std::vector<std::shared_ptr<Tile>> GameEngine::generateTempBag() {
    std::vector<std::shared_ptr<Tile>> tempBag;
    
    for (int i = 0; i < 20; i++) {
        for (const TileColor& tc : {RED, YELLOW, DARK_BLUE, LIGHT_BLUE, BLACK}) {
            std::shared_ptr<Tile> tile = nullptr;
            tile.reset(new Tile(tc));
            tempBag.push_back(tile);
        }
    }
    return tempBag;
}

void GameEngine::generateFactories() {
    int factoryCount = 0;

    if (numOfPlayers == 2) {
        factoryCount = 6;
    } else if (numOfPlayers == 3) {
        factoryCount = 8;
    } else if (numOfPlayers == 4) {
        factoryCount = 10;
    }

    for (int i = 0; i < factoryCount; i++) {
        factories.push_back(std::make_shared<Factory>());
    }
    fillFactories();
}

void GameEngine::fillFactories() {
    int size = factories.size();
    for (int i = 0; i < size; i++) {
         if (i == 0) {
            factories[i]->addTile(std::make_shared<Tile>(FIRST_TOKEN));
        }
        if (i > 0) {
            for(int j = 0; tileBag->size() && j < 4; j++) {
                factories[i]->addTile(tileBag->popFront());
            }
        }
    }
}

bool GameEngine::factoriesAreEmpty() {
    bool returnValue = false;
    int sumFacSize = 0;
    for (long unsigned int i = 0; i < factories.size(); i++) {
        sumFacSize += factories[i]->size();
    }
    if (sumFacSize > 0) {
        returnValue = false;
    } else {
        returnValue = true;
    }

    return returnValue;
}

std::vector<std::shared_ptr<Tile>> GameEngine::pickFactory(int index, char colour) {
    return factories[index]->getColorGroup(charToColour(colour));
}

TileColor GameEngine::charToColour(char colour) {
    TileColor returnValue = EMPTY;
    if (colour == 'R') {
        returnValue = RED;
    } else if (colour == 'Y') {
        returnValue = YELLOW;
    } else if (colour == 'B') {
        returnValue = DARK_BLUE;
    } else if (colour == 'L') {
        returnValue = LIGHT_BLUE;
    } else if (colour == 'U') {
        returnValue = BLACK;
    }
    return returnValue;
}

void GameEngine::endOfRound() {
    int size = players.size();
    for (int i = 0; i < size; i ++) {
        players[i]->endOfRound(*(tileBag));
    }
    fillFactories();
}

bool GameEngine::verifyTurnInputs(unsigned int factoryIndex, char tileChar,
                      unsigned int lineIndex) {

    bool success = false;

    try {
        if (factoryIndex > FACTORY_MAX_INDEX) {
            throw std::out_of_range("Factory index too high");
        }

        // Tile(char) will throw std::invalid_argument if tileChar is not valid.
        Tile t = Tile(tileChar);
        
        if (lineIndex > PLAYER_ROWS) {
            throw std::out_of_range("Line index too high");
        }

        success = true;
    } catch (std::logic_error& e) {
        std::cout << "Invalid turn input." << std::endl;
    };
    
    return success;
}

void GameEngine::addToCenterFactory(int choice){
    for (const TilePtr& tile : factories[choice]->getAll()) {
        factories[0]->addTile(tile);
    }
    factories[choice]->clear();
}

bool GameEngine::skipStdIO() {
    return (testMode || loadMode);
}

void GameEngine::goToNextPlayer(unsigned int& playerIndex) {
    if(playerIndex == 0) {
        playerIndex = 1;
    } else {
        playerIndex = 0;
    }
}