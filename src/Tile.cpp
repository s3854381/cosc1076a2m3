#include "Tile.h"

Tile::Tile(TileColor _color){
    //constructor
    this->color = _color;
}

Tile::Tile(char _color) {
    std::string validTiles = COLOR_CHARS;
    
    if (validTiles.find_first_of(_color) == std::string::npos)
    {
        std::string message = "Invalid tile character: ";
        message += _color;
        throw std::invalid_argument(message);
    }

    this->color = static_cast<TileColor>(_color);
}

Tile::Tile(const Tile& other) :
    color(other.color)
{}

Tile::Tile(Tile&& other){
    color = other.color;
}

Tile::~Tile(){
    //Deconstructor.
}

void Tile::setColor(TileColor color){
    this->color = color;
}

TileColor Tile::getTile(){
    return color;
}

char Tile::toChar() const {
    return static_cast<char>(color);
}


bool Tile::operator==(const Tile& other) const {
    return color == other.color;
}

bool Tile::operator< (const Tile& other) const {
    return toChar() < other.toChar();
}

bool Tile::operator!=(const Tile& other) const {
    return !(*this == other);
    }

bool Tile::operator> (const Tile& other) const {
    return other < *this;
}

bool Tile::operator<=(const Tile& other) const {
    return !(*this > other);
}

bool Tile::operator>=(const Tile& other) const {
    return !(*this < other);
}