#ifndef PLAYER_BOARD_H
#define PLAYER_BOARD_H

#include <vector>
#include <array>
#include <memory>
#include "TileBag.h"
#define MOSAIC_ROWS         5
#define MOSAIC_COLUMNS      5
#define PATTERN_LINES_ROWS  MOSAIC_ROWS
#define PLAYER_ROWS         PATTERN_LINES_ROWS + 1

/**
 * This class is responsible for tracking the state of the player's board.
 */
class PlayerBoard {
public:
    PlayerBoard();
    PlayerBoard(char templateType);
    PlayerBoard(const PlayerBoard& other);
    PlayerBoard(PlayerBoard&& other);
    ~PlayerBoard();

    /**
     * Adds tiles of the specified amount and colour to a pattern line.
     * Returns true if the parameters specified are valid to be added to the pattern line.
     * Returns false if the parameters are not valid to be added to the pattern line. 
     */
    void patternLineAdd(unsigned int row, std::vector<std::shared_ptr<Tile>>& colourGroup);
    /**
     * Adds a tile to the floor line.
     */
    void floorLineAdd(std::vector<std::shared_ptr<Tile>>& colourGroup);
    /**
     * Resets the playerMosaic to the default empty state.
     */
    void resetPlayerMosaic();
    /**
     * Resets the patternLines to the default empty state.
     */
    void resetPatternLines();
    /**
     * Returns the current state of the playerMosaic.
     */
    std::array<std::array<std::shared_ptr<Tile>, MOSAIC_COLUMNS>, MOSAIC_ROWS> getPlayerMosaic();
    /**
     * Returns the board template in use.
     */
    std::array<std::array<std::shared_ptr<Tile>, MOSAIC_COLUMNS>, MOSAIC_ROWS> getTemplate();
    /**
     * Returns the current state of the patternLines.
     */
    std::array<std::vector<std::shared_ptr<Tile>>, PATTERN_LINES_ROWS> getPatternLines();
    /**
     * Returns the current state of the floor line.
     */
    std::vector<std::shared_ptr<Tile>> getFloorLine();
    /**
     * Evaluates the board state and moves any full pattern lines to the playerMosaic then 
     * resets the necessary pattern lines and the floor line.
     * Returns the number of full pattern lines moved to the playerMosaic.
     */
    int endOfRound(TileBag& TileBag);
    /**
     * Checks if a tile is contained with a row of the player mosaic.
     */
    bool inMosaicRow(TileColor colour, unsigned int row);
    /**
     * Finds the index of the Tile with colour "colour" in the specified row of the patternLines.
     */
    int tilePosition(TileColor colour, int row);
    /**
     * Returns true if the floor line contains the first token.
     * Returns false if the floor line does not contain the first token.
     */
    bool firstTokenCheck();
    /**
     * Returns true if the pattern line at "row" is not full.
     * Returns false if the pattern at "row" is full
     * Precondition: GameEngine.verifyTurnInput() has returned True for these inputs.
     */
    bool validPatternLine(unsigned int row, TileColor color);
    /**
     * Returns the number of free tiles in the pattern line given by row.
     */
    unsigned int freeSpaceInLine(unsigned int row);
    /**
     * Returns true if the pattern line given by row has at least one tile in it.
     */
    bool patternLineStarted(unsigned int row);

private:
    std::array<std::array<std::shared_ptr<Tile>, MOSAIC_COLUMNS>, MOSAIC_ROWS> playerMosaic;
    std::array<std::array<std::shared_ptr<Tile>, MOSAIC_COLUMNS>, MOSAIC_ROWS> mosaicTemplate;
    const std::array<unsigned int, PATTERN_LINES_ROWS> maxSizeAtIndex = {1,2,3,4,5};
    std::array<std::vector<std::shared_ptr<Tile>>, PATTERN_LINES_ROWS> patternLines;
    std::vector<std::shared_ptr<Tile>> floorLine;

    void initialiseTemplate(char templateType);

    int countUp(int row, int column);

    int countDown(int row, int column);

    int countLeft(int row, int column);

    int countRight(int row, int column);

    int countVertical(int row, int column);

    int countHorizontal(int row, int column);
};

#endif //PLAYER_BOARD_H