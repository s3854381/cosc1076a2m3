#include "AzulStdOut.h"

AzulStdOut::AzulStdOut() {}

AzulStdOut::AzulStdOut(const AzulStdOut& other) {}

AzulStdOut::AzulStdOut(AzulStdOut&& other) {}

AzulStdOut::~AzulStdOut() {}

bool AzulStdOut::saveGame(const SavedGame& savedGame, std::string filename) {
    bool success = false;
    std::ofstream outFile(filename);

    writeTileBag(outFile, savedGame.getInitialTileBag());
    writePlayerNames(outFile, savedGame.getPlayerNames());
    writeTurns(outFile, savedGame.getTurns());

    outFile.close();
    success = true;
    std::cout << "File " << filename << " successfully saved." << std::endl;

    return success;
}

void AzulStdOut::writeTileBag(std::ofstream& ofstream, const TileVector& tileBag) {
    for (const Tile& tile : tileBag)
    {
        ofstream << tile.toChar();
    }
    ofstream << std::endl;
}

void AzulStdOut::writePlayerNames(std::ofstream& ofstream, const StringVector& playerNames) {
    for (const std::string& name : playerNames)
    {
        ofstream << name << std::endl;
    }
}

void AzulStdOut::writeTurns(std::ofstream& ofstream, const TurnVector& turns)  {
    for (const Turn& turn : turns)
    {
        ofstream << "turn ";
        ofstream << turn.getFactory() << " ";
        ofstream << turn.getTileChar() << " ";
        ofstream << turn.getPlayerLine() << std::endl;
    }
}

void AzulStdOut::outputTileBag(TileBag& tileBag) {
    std::shared_ptr<TileVector> vector = tileBag.getVector();
    std::cout << "TileBag: ";
    for (unsigned int i = 0; i < vector->size(); ++i) {
        std::cout << vector->at(i).toChar();
    }
    println();
}

void AzulStdOut::outputFactories(std::vector<std::shared_ptr<Factory>>& factories) {
    println("Factories: ");
    for(unsigned int i = 0; i < factories.size(); ++i) {
        std::cout << i << ": ";
        for(TilePtr tile : factories[i]->getAll()) {
            std::cout << tile->toChar() << " ";
        }
        println();
    }
    println();
}

void AzulStdOut::outputPlayerBoard(Player& player) {
    std::string name = player.getName();
    outputPlayerScore(player);

    std::cout << "Mosaic for " << name << ":" << std::endl;
    for (int i = 0; i < MOSAIC_ROWS; i++) {
        std::cout << i + 1 << ": ";

        // Prints the pattern line.
        int numWhiteSpaces = (MOSAIC_COLUMNS - 1 - i) * 2;
        for (int j = 0; j < numWhiteSpaces; ++j) {
            std::cout << " ";
        }
        
        int emptySpace = (i + 1) - player.getPatternLines()[i].size();
        for(int j = 0; j < emptySpace; ++j) {
            std::cout << ". ";
        }

        for(unsigned int x = 0; x < player.getPatternLines()[i].size(); ++x){
            std::cout << player.getPatternLines()[i][x]->toChar() << " ";
        }

        // Prints the center bar
        std::cout << "||";
        for (int j = 0; j < MOSAIC_COLUMNS; j++) {
            std::cout << " " << player.getPlayerMosaic()[i][j]->toChar();
        }
        println();
    }
    // Print out the floor line
    std::cout << "broken: ";
    for(unsigned int i = 0; i < player.getFloorLine().size(); ++i) {
        std::cout << player.getFloorLine()[i]->toChar() << " ";
    }
    println();
    println();
}

void AzulStdOut::outputPlayerScore(Player& player) {
    std::cout << "Score for Player " << player.getName() << ": " << player.getPoints() << std::endl;
}


void AzulStdOut::outputEndGame(std::vector<std::shared_ptr<Player>>& players) {
    println("=== GAME OVER ===");
    println();
    println("Final Scores: ");
    std::string player0Name = players[0]->getName();
    std::string player1Name = players[1]->getName();
    int player0Points = players[0]->getPoints();
    int player1Points = players[1]->getPoints();

    std::cout << "Player " << player0Name << ": " << player0Points << std::endl;
    std::cout << "Player " << player1Name << ": " << player1Points << std::endl;
    if(player0Points > player1Points){
        std::cout << "Player " << player0Name << " wins ! " << std::endl;
    } else if (player1Points > player0Points){
        std::cout << "Player " << player1Name << " wins ! " << std::endl;
    } else {
        println("Draw !");
    }
    println();
}

void AzulStdOut::printCredits() {
    println("-----------------------------");
    println("Name: Michael Asquith");
    println("Student ID: S3854381");
    println("Email: S3854381@student.rmit.edu.au");
    println();
    println("Name: Ronald Tang");
    println("Student ID: S3725095");
    println("Email: S3725095@student.rmit.edu.au");
    println();
    println("Name: Sean Wong");
    println("Student ID: S3786110");
    println("Email: S3786110@student.rmit.edu.au");
    println("-----------------------------");
    println();
}

void AzulStdOut::printMenu() {
    println("Welcome to Azul!");
    println("--------------------");
    println();
    println("Menu");
    println("----");
    println("1. New Game");
    println("2. Load Game");
    println("3. Credits");
    println("4. Quit");
    println();
}