#include "SavedGame.h"

SavedGame::SavedGame(const TileBag& initialTileBag) {
    this->initialTileBag = initialTileBag.getVector();
    turns = std::make_shared<TurnVector>();
    players = std::make_shared<StringVector>();
}

SavedGame::SavedGame(std::shared_ptr<TileVector> initialTileBag) {
    this->initialTileBag = initialTileBag;
    turns = std::make_shared<TurnVector>();
    players = std::make_shared<StringVector>();
}

SavedGame::SavedGame(const SavedGame& other) {
    initialTileBag = std::make_shared<TileVector>(*other.initialTileBag);
    turns = std::make_shared<TurnVector>(*other.turns);
    players = std::make_shared<StringVector>(*other.players);
}

SavedGame::SavedGame(SavedGame&& other) {
    initialTileBag = other.initialTileBag;
    turns = other.turns;
    players = other.players;
    other.initialTileBag = nullptr;
    other.turns = nullptr;
    other.players = nullptr;
}

SavedGame::~SavedGame() {
    initialTileBag = nullptr;
    players = nullptr;
    turns = nullptr;
}

void SavedGame::addPlayer(std::string playerName) {
    players->push_back(playerName);
}

void SavedGame::addTurn(int factoryNum, char tileChar, int playerLine) {
    Turn turn = Turn(factoryNum, tileChar, playerLine);

    // Transfer turn object to Linked List.
    turns->push_back(turn);
}

void SavedGame::addTurn(Turn turn) {
    turns->push_back(turn);
}

const TileVector& SavedGame::getInitialTileBag() const {
    return *initialTileBag;
}

const StringVector& SavedGame::getPlayerNames() const {
    return *players;
}

const TurnVector& SavedGame::getTurns() const {
    return *turns;
}