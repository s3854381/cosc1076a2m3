#ifndef GAME_ENGINE_H
#define GAME_ENGINE_H

#include <vector>
#include <memory>
#include <random>
#include <algorithm>

#include "AIStrategy.h"
#include "AzulStdIO.h"
#include "SavedGame.h"
#include "Factory.h"
#include "TileBag.h"
#include "Player.h"
#include "Turn.h"

#define MAX_ROUNDS          5
#define FACTORY_MAX_INDEX   6
#define AI_PLAYER_NUMBER    1

/**
 * The Player class is responsible for the logic and the execution of the game logic.
 */
class GameEngine {
public:
    GameEngine();
    GameEngine(bool aiMode);
    GameEngine(int players, std::shared_ptr<SavedGame> save, bool testMode);
    GameEngine(const GameEngine& other);
    GameEngine(GameEngine&& other);
    ~GameEngine();
    
    /**
     * Adds a player to the game Engine.
     */
    void addPlayer(std::string name);
    /**
     * Starts the game engine.
     */
    bool startGame(AzulStdIO& azulStdIO);
    /**
     * Loads a game from a file specified in AzulStdIO.
     */
    bool loadGame(AzulStdIO& azulStdIO);
    /**
     * Executes a player turn. METHOD NOT CURRENTLY IN GAMEENGINE
     */
    void playerTurn(Player& player, int factory, char tile, int row);
    /**
     * Called at the end of the round to reset and prepare the player's board for the next round of play.
     */
    void endOfRound();
    /**
     * Function used to generate a random tileBag with a specific seed.
     */
    void generateTileBag(int seed);
    /**
     * Instantiates the factory objects based on the number of players.
     */
    void generateFactories();
    /**
     * Fills all the factories with Tile objects from the tileBag object.
     */
    void fillFactories();
    /**
     * Returns a temporary tileBag as a vector to facilitate the shuffling of the tile bag.
     */
    std::vector<std::shared_ptr<Tile>> generateTempBag();
    /**
     * Returns true if all the factories besides the central factory (index > 0) are empty
     */
    bool factoriesAreEmpty();
    /**
     * Adds all the tiles of the selected colour from the selected factory to the player's 'hand'
     */
    std::vector<std::shared_ptr<Tile>> pickFactory(int index, char colour);
    /**
     * Converts the input from type char to type TileColor.
     */
    TileColor charToColour(char colour);
    /**
     * Verify's the itegrity of the turn inputs to see if they're valid.
     */
    bool verifyTurnInputs(unsigned int factoryIndex, char tileChar,
                      unsigned int lineIndex);
    /**
     * Adds remaining/unselected factory tiles to the center factory.
     */
    void addToCenterFactory(int choice);


private:
    std::vector<std::shared_ptr<Player>> players;
    std::shared_ptr<SavedGame> savedGame;
    std::vector<std::shared_ptr<Factory>> factories;
    std::shared_ptr<TileBag> tileBag;
    std::shared_ptr<AzulStdIO> input;
    int rounds = 1;
    char templateType;
    bool testMode;
    bool loadMode;
    bool aiMode;
    int numOfPlayers;
    /**
    * Skips the input/ouput sections of the StartGame() method,
    * if running in testing mode or load game.
    */
    bool skipStdIO();

    /**
     * Changes playerIndex to the next player for their turn.
     */
    void goToNextPlayer(unsigned int& playerIndex);
};

#endif //GAME_ENGINE_H