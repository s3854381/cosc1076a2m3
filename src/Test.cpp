#include "Factory.h"
#include "Tile.h"
#include "TileBag.h"
#include "Test.h"
#include "PlayerBoard.h"
#include "GameEngine.h"
#include "AzulStdIO.h"
#include "SavedGame.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <memory>
#include <stdexcept>

void printFoundation(std::shared_ptr<TileBag> fileTileBag, std::shared_ptr<SavedGame> save);
std::shared_ptr<TileBag> convertVectorToTileBag(std::shared_ptr<SavedGame> save);

void testGame(std::string filePath){

    std::shared_ptr<AzulStdIO> stdIn = std::make_shared<AzulStdIO>();
    std::shared_ptr<SavedGame> testSave = stdIn->loadGame(filePath);
    //Initialise Game.
    std::shared_ptr<GameEngine> ge = nullptr;
    ge.reset(new GameEngine(2, testSave, true));
    //Start the game from the GameEngine to run in Testing Mode.
    ge->startGame(*stdIn);
}