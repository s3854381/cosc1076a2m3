Instructions:

"./main" to play the game with 2 human players.
"./main -t" to open in testing mode.
"/.main --ai" to play the game vs the Shallow Blue AI.

"turn <F> <T> <R>" Takes all tiles matching <T> from factory <F> and puts them in your mosaic row <R>.
"save <filename>" Saves the game to <filename>.
"quit" Quits the game.