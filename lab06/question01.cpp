#include <vector>
#include <iostream>

int main(void)
{
    /**
    * Write a C++ program that:
    * (a) Creates a C++ STL Vector of integers
    * (b) Fills the vector with 100 integers from 0 to 99
    * (c) Prints out the contents of the vector as a comma separated list on a single line
    */
    
    std::vector<int> v1;

    for(int i = 0; i < 100; ++i)
    {
        v1.push_back(i);
        v1[0];
    }
    std::cout << "Output: " << std::endl;
    
    for(unsigned int i = 0; i < v1.size(); ++i)
    {
        std::cout << v1[i];
        
        if (i != v1.size() - 1) {
            std::cout << ", ";
        } else
        {
            std::cout << std::endl;
        }
        
    }

    return EXIT_SUCCESS;
}