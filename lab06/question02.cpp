#include <iostream>

#include "LinkedList.h"

int main(void) {
    LinkedList ll1 = LinkedList();

    for(int i = 0; i < 3; ++i)
    {
        ll1.addFront(i);
    }

    for(int i = 0; i < 3; ++i)
    {
        std::cout << ll1.get(i) << std::endl;
    }

    return EXIT_SUCCESS;
}